import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  Home,
  Splash,
  Profile,
  ListJersey,
  JerseyDetail,
  Keranjang,
  Checkout,
  EditProfile,
  ChangePassword,
  History,
  Login,
  Register1,
  Register2,
  DisplayTempatTidur,
  MonitorAntrean,
  JadwalDokter,
  KuotaPoli,
  FasilitasPelayanan,
  KontakKami,
  DaftarPasienJkn,
  DaftarPasienNonJkn,
  NoRujukanPeserta,
} from '../pages';
import {BottomNavigator} from '../components/molecules';
import RegisterRawatJalan from '../pages/RegisterRawatJalan';
import ListKuotaPoli from '../pages/ListKuotaPoli';
import ListJadwalDokter from '../pages/ListJadwalDokter';
import ListJadwalDokterPoli from '../pages/ListJadwalDokterPoli';
import HistoryBooking from '../pages/HistoryBooking';
import PembatalanAntrean from '../pages/PembatalanAntrean';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

function MainApp() {
  return (
    <Tab.Navigator
      screenOptions={{headerShown: false}}
      tabBar={props => <BottomNavigator {...props} />}>
      <Tab.Screen name="Home" component={Home} />
      {/* <Tab.Screen
        name="ListJersey"
        component={ListJersey}
        options={{title: 'Jersey'}}
      /> */}
      <Tab.Screen name="History" component={HistoryBooking} />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );
}

const Router = () => {
  return (
    // screenOptions={{headerShown: false}}
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="JerseyDetail"
        component={JerseyDetail}
        options={{headerShown: false}}
      />

      <Stack.Screen name="Keranjang" component={Keranjang} />

      <Stack.Screen name="Checkout" component={Checkout} />

      <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{title: 'Edit Profile'}}
      />

      <Stack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={{title: 'Change Password'}}
      />

      <Stack.Screen
        name="RegisterRawatJalan"
        component={RegisterRawatJalan}
        options={{title: 'Registrasi Rawat Jalan'}}
      />
      <Stack.Screen
        name="DaftarPasienJkn"
        component={DaftarPasienJkn}
        options={{title: 'Daftar Pasien Jkn'}}
      />
      <Stack.Screen
        name="DaftarPasienNonJkn"
        component={DaftarPasienNonJkn}
        options={{title: 'Daftar Pasien Non Jkn'}}
      />
      <Stack.Screen
        name="NoRujukanPeserta"
        component={NoRujukanPeserta}
        options={{title: 'No Rujukan Peserta'}}
      />
      <Stack.Screen
        name="DisplayTempatTidur"
        component={DisplayTempatTidur}
        options={{title: 'Display Tempat Tidur'}}
      />
      <Stack.Screen
        name="MonitorAntrean"
        component={MonitorAntrean}
        options={{title: 'Monitor Antrean Poli'}}
        // options={{headerShown: false}}
      />
      <Stack.Screen
        name="PembatalanAntrean"
        component={PembatalanAntrean}
        options={{title: 'Pembatalan Antrean Poli'}}
      />
      <Stack.Screen
        name="JadwalDokter"
        component={JadwalDokter}
        options={{title: 'Jadwal Dokter Praktek'}}
      />
      <Stack.Screen
        name="ListJadwalDokter"
        component={ListJadwalDokter}
        options={{title: 'List Jadwal Dokter'}}
      />
      <Stack.Screen
        name="ListJadwalDokterPoli"
        component={ListJadwalDokterPoli}
        options={{title: 'List Jadwal Dokter'}}
      />
      <Stack.Screen
        name="KuotaPoli"
        component={KuotaPoli}
        options={{title: 'Kuota Poli'}}
      />
      <Stack.Screen
        name="ListKuotaPoli"
        component={ListKuotaPoli}
        options={{title: 'List Kuota Poli'}}
      />
      <Stack.Screen
        name="FasilitasPelayanan"
        component={FasilitasPelayanan}
        options={{title: 'Fasilitas Pelayanan'}}
      />
      <Stack.Screen
        name="KontakKami"
        component={KontakKami}
        options={{title: 'Kontak Kami'}}
      />

      <Stack.Screen
        name="History"
        component={History}
        options={{title: 'History Pemesanan'}}
      />

      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="Register1"
        component={Register1}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="Register2"
        component={Register2}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Router;
