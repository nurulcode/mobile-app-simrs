import SerieA from './seriea.png';
import LaLiga from './laliga.png';
import PremierLeague from './premierleague.png';
import BundesLiga from './bundesliga.png';
import Folder from './folder.png';
import Registration from './registration.png';
import PatientPoli from './patient-poli.png';
import Doctor from './doctor.png';
import Patient from './patient.png';
import Bed from './hospital-bed.png';
import Bed2 from './hospital-bed2.png';
import IndonesiaRupiah from './indonesian-rupiah.png';
import MedicalHistory from './medical-history.png';
import MedicalHistory2 from './medical-history2.png';
import HealthCare from './health-care.png';
import MedicalTeam from './medical-team.png';

export {
  BundesLiga,
  LaLiga,
  PremierLeague,
  SerieA,
  Folder,
  Registration,
  PatientPoli,
  Doctor,
  Patient,
  Bed,
  Bed2,
  IndonesiaRupiah,
  MedicalHistory,
  MedicalHistory2,
  HealthCare,
  MedicalTeam,
};
