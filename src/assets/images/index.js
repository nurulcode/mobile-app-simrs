import Logo from './logo.svg';
import Logo2 from './logo.png';
import LogoNew from './logoNew.svg';
import Ilustrasi from './ilustrasi.js';
import Slider1 from './slider1.png';
import Slider2 from './slider2.png';
import Slider11 from './slider1.jpg';
import Slider22 from './slider2.jpeg';
import Slider33 from './slider3.jpeg';
import FotoProfile from './profile.jpg';
import IlustrasiRegister1 from './register1.js';
import IlustrasiRegister2 from './register2.js';
import DefaultImage from './default.jpg';

export {
  Logo,
  Ilustrasi,
  Slider1,
  Slider2,
  Slider11,
  Slider22,
  Slider33,
  FotoProfile,
  IlustrasiRegister1,
  IlustrasiRegister2,
  DefaultImage,
  LogoNew,
  Logo2,
};
