export const dummyJadwalDokter = {
  response: {
    list: [
      {
        id: 3,
        hari: 'Senin',
        mulai: '08:00:00',
        selesai: '12:00:00',
        kuota: 40,
      },
      {
        id: 4,
        hari: 'Selasa',
        mulai: '08:00:00',
        selesai: '12:00:00',
        kuota: 40,
      },
      {
        id: 5,
        hari: 'Rabu',
        mulai: '08:00:00',
        selesai: '12:00:00',
        kuota: 40,
      },
      {
        id: 6,
        hari: 'Kamis',
        mulai: '08:00:00',
        selesai: '12:00:00',
        kuota: 40,
      },
      {
        id: 7,
        hari: 'Jumat',
        mulai: '08:00:00',
        selesai: '12:00:00',
        kuota: 40,
      },
      {
        id: 8,
        hari: 'Sabtu',
        mulai: '08:00:00',
        selesai: '12:00:00',
        kuota: 40,
      },
    ],
  },
};
