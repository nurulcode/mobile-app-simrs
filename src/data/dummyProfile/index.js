import { FotoProfile } from "../../assets";

export const dummyProfile = {
    nama: 'Nurul Hidayat',
    email: 'papuatampan@gmail.com',
    nomerHp: '082331860289',
    alamat: 'Jl. Bhayangkara No. 1 Manokwari',
    kota: 'Manokwari',
    provinsi: 'Papua Barat',
    avatar: FotoProfile
};