export const dummyPoliByJadwal = {
  metadata: {
    code: 200,
    message: 'Ok',
  },
  response: [
    {
      id: 1,
      uraian: 'Poliklinik Interna',
    },
    {
      id: 8,
      uraian: 'Poliklinik Obstetri dan Gynekologi',
    },
  ],
};
