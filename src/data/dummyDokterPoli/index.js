export const dummyDokterPoli = {
  response: [
    {
      id: 1,
      uraian: 'Poliklinik Interna',
      nama: 'DR. FRANSISCUS AGUSTINUS WABIA, SP.PD',
    },
    {
      id: 2,
      uraian: 'Poliklinik Interna',
      nama: 'DR. FANY OKTARINA TAHA, SP.PD',
    },
    {
      id: 3,
      uraian: 'Poliklinik Obstetri dan Gynekologi',
      nama: 'DR. WAHYURIDISTIA MARHENRIYANTO. SP.OG',
    },
    {
      id: 23,
      uraian: 'Poliklinik Obstetri dan Gynekologi',
      nama: 'DR. RIVALDI DOMINGGUS LILIGOLY, SP.OG',
    },
  ],
};
