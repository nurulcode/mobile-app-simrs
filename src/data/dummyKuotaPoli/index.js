export const dummyKuotaPoli = {
  metadata: {
    code: 200,
    message: 'Ok',
  },
  response: {
    tanggal_periksa: 'Sabtu, 14 Januari 2023',
    poli: 'Poliklinik Obstetri dan Gynekologi',
    hari: 'Selasa',
    list: [
      {
        nama: 'DR. WAHYURIDISTIA MARHENRIYANTO. SP.OG',
        kuota: 40,
        sisa_kuota: 40,
        terdaftar: 0,
      },
      {
        nama: 'DR. RIVALDI DOMINGGUS LILIGOLY, SP.OG',
        kuota: 20,
        sisa_kuota: 20,
        terdaftar: 0,
      },
    ],
  },
};
