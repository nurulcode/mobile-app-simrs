import axios from 'axios';
import {GET_KOTA, GET_KOTA_DETAIL, GET_PROVINSI} from '../constants/RajaOngkirConstat';
import {
  API_HEADER_RAJAONGKIR,
  API_RAJAONGKIR,
  API_TIMEOUT,
  API_KEY,
} from '../utils';

import {
  dispatchError,
  dispatchLoading,
  dispatchSuccess,
  storeData,
} from '../utils';

export const getProvinsiList = () => {
  return dispatch => {
    // LOADING
    dispatchLoading(dispatch, GET_PROVINSI);

    axios({
      method: 'GET',
      url: API_RAJAONGKIR + 'province',
      timeout: API_TIMEOUT,
      headers: API_HEADER_RAJAONGKIR,
    })
      .then(response => {
        if (response.status !== 200) {
          dispatchSuccess(dispatch, GET_PROVINSI, false);
        } else {
          const data = response.data ? response.data.rajaongkir.results : [];
          dispatchSuccess(dispatch, GET_PROVINSI, data);
        }
      })
      .catch(error => {
        dispatchError(dispatch, GET_PROVINSI, error);
        alert(error);
      });
  };
};

export const getKotaList = provinsi_id => {
  return dispatch => {
    // LOADING
    dispatchLoading(dispatch, GET_KOTA);
    axios({
      method: 'GET',
      url: API_RAJAONGKIR + 'city?province=' + provinsi_id,
      timeout: API_TIMEOUT,
      headers: API_HEADER_RAJAONGKIR,
    })
      .then(response => {
        if (response.status !== 200) {
          dispatchSuccess(dispatch, GET_KOTA, false);
        } else {
          const data = response.data ? response.data.rajaongkir.results : [];
          dispatchSuccess(dispatch, GET_KOTA, data);
        }
      })
      .catch(error => {
        dispatchError(dispatch, GET_KOTA, error);
        alert(error);
      });
  };
};

export const getKotaDetail = kota_id => {
  return dispatch => {
    // LOADING
    dispatchLoading(dispatch, GET_KOTA_DETAIL);
    axios({
      method: 'GET',
      url: API_RAJAONGKIR + 'city?id=' + kota_id,
      timeout: API_TIMEOUT,
      headers: API_HEADER_RAJAONGKIR,
    })
      .then(response => {
        if (response.status !== 200) {
          dispatchSuccess(dispatch, GET_KOTA_DETAIL, false);
        } else {
          const data = response.data ? response.data.rajaongkir.results : [];
          dispatchSuccess(dispatch, GET_KOTA_DETAIL, data);
        }
      })
      .catch(error => {
        dispatchError(dispatch, GET_KOTA, error);
        alert(error);
      });
  };
};
