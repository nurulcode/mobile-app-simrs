import axios from 'axios';

import {
  LOGIN_USER_ERROR,
  LOGIN_USER_LOADING,
  LOGIN_USER_SUCCESS,
  PROFILE_ERROR,
  PROFILE_LOADING,
  PROFILE_SUCCESS,
  REGISTER_USER_ERROR,
  REGISTER_USER_LOADING,
  REGISTER_USER_SUCCESS,
} from '../constants/AuthConstat';

import {
  API_HEADER_WS,
  dispatchError,
  dispatchLoading,
  dispatchSuccess,
  storeData,
  URL_MOBILE_APP,
} from '../utils';

export const loginUser = (norm, tgl) => {
  return dispatch => {
    dispatchLoading(dispatch, LOGIN_USER_LOADING);

    axios
      .post(URL_MOBILE_APP + '/login', {
        no_rekam_medis: norm,
        tgl_lahir: tgl,
      })
      .then(response => {
        if (response.data.metadata.code != 200) {
          dispatchError(
            dispatch,
            LOGIN_USER_ERROR,
            response.data.metadata.message,
          );
        } else {
          const data = response.data.response;
          storeData('userInfo', data);
          dispatchSuccess(dispatch, LOGIN_USER_SUCCESS, response.data.response);
        }
      })
      .catch(error => {
        dispatchError(dispatch, LOGIN_USER_ERROR, error);
        console.log(error);
        alert(error);
      });
  };
};

export const getProfile = noRujukan => async (dispatch, getState) => {
  dispatchLoading(dispatch, PROFILE_LOADING);

  const {
    UserLoginReducer: {userInfo},
  } = getState();

  const dataLogin = await userInfo.then(data => {
    return data;
  });

  await axios({
    method: 'GET',
    url: URL_MOBILE_APP + '/me',
    headers: {Authorization: `Bearer ${dataLogin.token}`},
  })
    .then(response => {
      if (response.data.metadata.code != 200) {
        dispatchError(dispatch, PROFILE_ERROR, response.data);
      } else {
        dispatchSuccess(dispatch, PROFILE_SUCCESS, response.data.response);
      }
    })
    .catch(error => {
      dispatchError(dispatch, PROFILE_ERROR, error);
      console.log(error);
      alert(error);
    });
};

// export const registerUser = (data, password) => {
//   return dispatch => {
//     dispatchLoading(dispatch, REGISTER_USER_LOADING);

//     FIREBASE.auth()
//       .createUserWithEmailAndPassword(data.email, password)
//       .then(userCredential => {
//         // Signed in
//         var user = userCredential.user;

//         const dataBaru = {
//           ...data,
//           uid: user.uid,
//         };

//         FIREBASE.database()
//           .ref('users/' + user.uid)
//           .set(dataBaru);

//         storeData('user', dataBaru);
//         dispatchSuccess(dispatch, REGISTER_USER_SUCCESS, dataBaru);
//       })

//       .catch(error => {
//         var errorMessage = error.message;
//         dispatchError(dispatch, REGISTER_USER_ERROR, errorMessage);
//       });
//   };
// };

// export const loginUser = (email, password) => {
//   return dispatch => {
//     dispatchLoading(dispatch, LOGIN_USER_LOADING);

//     FIREBASE.auth()
//       .signInWithEmailAndPassword(email, password)
//       .then(userCredential => {
//         // Signed in
//         var user = userCredential.user;
//         FIREBASE.database()
//           .ref('/users/' + user.uid)
//           .once('value')
//           .then(response => {
//             if (response) {
//               storeData('user', response.val());
//               dispatchSuccess(dispatch, LOGIN_USER_SUCCESS, response.val());
//             } else {
//               dispatchError(dispatch, LOGIN_USER_ERROR, 'Data user tidak ada');
//               alert('Data user tidak ada!');
//             }
//           });
//       })
//       .catch(error => {
//         var errorCode = error.code;
//         var errorMessage = error.message;
//         dispatchError(dispatch, LOGIN_USER_ERROR, errorMessage);
//         alert(errorMessage);
//       });
//   };
// };
