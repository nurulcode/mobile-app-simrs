import FIREBASE from '../config/FIREBASE';
import {
  KERANJANG_DATA,
  KERANJANG_ERROR,
  KERANJANG_LOADING,
  KERANJANG_SUCCESS,
  LIST_KERANJANG_DATA,
  LIST_KERANJANG_ERROR,
  LIST_KERANJANG_LOADING,
  LIST_KERANJANG_SUCCESS,
  REMOVE_KERANJANG_ERROR,
  REMOVE_KERANJANG_LOADING,
  REMOVE_KERANJANG_SUCCESS,
} from '../constants/KeranjangConstat';
import {
  dispatchData,
  dispatchError,
  dispatchLoading,
  dispatchSuccess,
  storeData,
} from '../utils';

export const masukKeranjang = data => {
  return dispatch => {
    dispatchLoading(dispatch, KERANJANG_LOADING);
    FIREBASE.database()
      .ref('keranjangs/' + data.id)
      .once('value')
      .then(response => {
        if (response.val()) {
          const keranjangUtama = response.val();
          const hargaBaru = parseInt(data.jumlah) * parseInt(data.jersey.harga);
          const beratBaru =
            parseInt(data.jumlah) * parseFloat(data.jersey.berat);

          FIREBASE.database()
            .ref('keranjangs')
            .child(data.id)
            .update({
              totalHarga: keranjangUtama.totalHarga + hargaBaru,
              totalBerat: keranjangUtama.totalBerat + beratBaru,
            })
            .then(response => {
              dispatch(masukKeranjangDetail(data));
            })
            .catch(error => {
              dispatchError(dispatch, KERANJANG_ERROR, error);
              alert(error);
            });
        } else {
          const keranjangUtama = {
            user: data.id,
            tanggal: new Date().toDateString(),
            totalHarga: parseInt(data.jumlah) * parseInt(data.jersey.harga),
            totalBerat: parseInt(data.jumlah) * parseFloat(data.jersey.berat),
          };

          FIREBASE.database()
            .ref('keranjangs')
            .child(data.id)
            .set(keranjangUtama)
            .then(response => {
              dispatch(masukKeranjangDetail(data));
            })
            .catch(error => {
              dispatchError(dispatch, KERANJANG_ERROR, error);
              alert('keranjangs' + error);
            });
        }
      });
  };
};

export const masukKeranjangDetail = data => {
  return dispatch => {
    const pesanans = {
      product: data.jersey,
      jumlahPesanan: data.jumlah,
      totalHarga: parseInt(data.jumlah) * parseInt(data.jersey.harga),
      totalBerat: parseInt(data.jumlah) * parseFloat(data.jersey.berat),
      keterangan: data.keterangan,
      ukuran: data.ukuran,
    };

    FIREBASE.database()
      .ref('keranjangs/' + data.id)
      .child('pesanans')
      .push(pesanans)
      .then(response => {
        dispatchData(dispatch, KERANJANG_SUCCESS, response ? response : []);

        dispatchSuccess(dispatch, KERANJANG_DATA, response ? response : []);
      })
      .catch(error => {
        dispatchError(dispatch, KERANJANG_ERROR, error);
        alert('pesanans ' + error);
      });
  };
};

export const getListKeranjang = id => {
  return dispatch => {
    dispatchLoading(dispatch, LIST_KERANJANG_LOADING);

    FIREBASE.database()
      .ref('keranjangs/' + id)
      .once('value')
      .then(res => {
        dispatchSuccess(
          dispatch,
          LIST_KERANJANG_SUCCESS,
          res.val() ? res.val() : false,
        );
      })
      .catch(error => {
        dispatchError(dispatch, LIST_KERANJANG_ERROR, error.message);
        alert(error.message);
      });
  };
};

export const removeCart = (id, keranjangUtama, keranjang) => {
  return dispatch => {
    dispatchLoading(dispatch, REMOVE_KERANJANG_LOADING);

    const totalHargaBaru = keranjangUtama.totalHarga - keranjang.totalHarga;
    const totalBeratBaru = keranjangUtama.totalBerat - keranjang.totalBerat;

    if (totalBeratBaru == 0 && totalHargaBaru == 0) {
      FIREBASE.database()
        .ref('keranjangs')
        .child(keranjangUtama.user)
        .remove()
        .then(response => {
          dispatchSuccess(
            dispatch,
            REMOVE_KERANJANG_SUCCESS,
            'Keranjang Sukses Dihapus',
          );
        })
        .catch(error => {
          dispatchError(dispatch, REMOVE_KERANJANG_ERROR, error);
          alert(error);
        });
    } else {
      FIREBASE.database()
        .ref('keranjangs')
        .child(keranjangUtama.user)
        .update({
          totalBerat: totalBeratBaru,
          totalHarga: totalHargaBaru,
        })
        .then(response => {
          dispatch(removeKeranjangDetail(id, keranjangUtama));
        })
        .catch(error => {
          dispatchError(dispatch, REMOVE_KERANJANG_ERROR, error);
          alert(error);
        });
    }
  };
};

export const removeKeranjangDetail = (id, keranjangUtama) => {
  return dispatch => {
    FIREBASE.database()
      .ref('keranjangs/' + keranjangUtama.user)
      .child('pesanans')
      .child(id)
      .remove()
      .then(response => {
        dispatchSuccess(
          dispatch,
          REMOVE_KERANJANG_SUCCESS,
          'Keranjang Sukses Dihapus',
        );
      })
      .catch(error => {
        dispatchError(dispatch, REMOVE_KERANJANG_ERROR, error);
        alert(error);
      });
  };
};
