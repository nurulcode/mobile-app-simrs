import axios from 'axios';
import {
  POLI_LOADING,
  POLI_SUCCESS,
  POLI_ERROR,
  DOKTER_LOADING,
  DOKTER_SUCCESS,
  DOKTER_ERROR,
} from '../constants/JadwalDokterPoliConstat';

import {
  API_HEADER_WS,
  dispatchError,
  dispatchLoading,
  dispatchSuccess,
  URL_MOBILE_APP,
} from '../utils';

export const getPoliVclaim = tgl => async (dispatch, getState) => {
  dispatchLoading(dispatch, POLI_LOADING);

  // const {
  //   UserLoginReducer: {userInfo},
  // } = getState();

  // const dataLogin = await userInfo.then(data => {
  //   return data;
  // });

  await axios({
    method: 'GET',
    url: URL_MOBILE_APP + '/poli/tgl/' + tgl,
    headers : API_HEADER_WS,
  })
    .then(response => {
      if (response.data.metadata.code != 200) {
        dispatchError(dispatch, POLI_ERROR, response.data.metadata.message);
      } else {
        dispatchSuccess(dispatch, POLI_SUCCESS, response.data.response);
      }
    })
    .catch(error => {
      dispatchError(dispatch, POLI_ERROR, error);
      console.log(error);
      alert(error);
    });
};
export const getDokterPoli = (poli, tgl) => async (dispatch, getState) => {
  dispatchLoading(dispatch, DOKTER_LOADING);
  await axios({
    method: 'GET',
    url: URL_MOBILE_APP + '/dokter/poli/' + poli + '/tgl/' + tgl,
    headers : API_HEADER_WS,
  })
    .then(response => {
      if (response.data.metadata.code != 200) {
        dispatchError(dispatch, DOKTER_ERROR, response.data.metadata.message);
      } else {
        dispatchSuccess(dispatch, DOKTER_SUCCESS, response.data.response);
      }
    })
    .catch(error => {
      dispatchError(dispatch, DOKTER_ERROR, error);
      console.log(error);
      alert(error);
    });
};
