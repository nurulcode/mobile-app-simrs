import axios from 'axios';
import {
  DTT_LOADING,
  DTT_SUCCESS,
  DTT_ERROR,
  ANTREAN_POLI_LOADING,
  ANTREAN_POLI_SUCCESS,
  ANTREAN_POLI_ERROR,
  KUOTA_POLI_LOADING,
  KUOTA_POLI_SUCCESS,
  KUOTA_POLI_ERROR,
  JADWAL_DOKTER_LOADING,
  JADWAL_DOKTER_SUCCESS,
  JADWAL_DOKTER_ERROR,
  JADWAL_DOKTER_DETAIL_LOADING,
  JADWAL_DOKTER_DETAIL_SUCCESS,
  JADWAL_DOKTER_DETAIL_ERROR,
  JADWAL_POLI_LOADING,
  JADWAL_POLI_SUCCESS,
  JADWAL_POLI_ERROR,
  JADWAL_POLI_DETAIL_LOADING,
  JADWAL_POLI_DETAIL_SUCCESS,
  JADWAL_POLI_DETAIL_ERROR,
} from '../constants/HomeConstat';

import {
  API_HEADER_WS,
  dispatchError,
  dispatchLoading,
  dispatchSuccess,
  storeData,
  URL_MOBILE_APP,
  URL_VCLAIM,
} from '../utils';

export const getDisplayTempatTidur = () => async (dispatch, getState) => {
  dispatchLoading(dispatch, DTT_LOADING);

  await axios({
    method: 'GET',
    url: URL_MOBILE_APP + '/ketersediaan-tempat-tidur',
    headers: API_HEADER_WS,
  })
    .then(response => {
      if (response.data.metadata.code != 200) {
        dispatchError(dispatch, DTT_ERROR, response.data.metadata.message);
      } else {
        dispatchSuccess(dispatch, DTT_SUCCESS, response.data.response);
      }
    })
    .catch(error => {
      dispatchError(dispatch, DTT_ERROR, error);
      console.log(error);
      alert(error);
    });
};

export const getAntreanPoli = () => async (dispatch, getState) => {
  dispatchLoading(dispatch, ANTREAN_POLI_LOADING);

  await axios({
    method: 'GET',
    url: URL_MOBILE_APP + '/monitor-antrean-poli',
    headers: API_HEADER_WS,
  })
    .then(response => {
      if (response.data.metadata.code != 200) {
        dispatchError(
          dispatch,
          ANTREAN_POLI_ERROR,
          response.data.metadata.message,
        );
      } else {
        dispatchSuccess(dispatch, ANTREAN_POLI_SUCCESS, response.data.response);
      }
    })
    .catch(error => {
      dispatchError(dispatch, ANTREAN_POLI_ERROR, error);
      console.log(error);
      alert(error);
    });
};

export const getJadwalDokter = () => async (dispatch, getState) => {
  dispatchLoading(dispatch, JADWAL_DOKTER_LOADING);

  await axios({
    method: 'GET',
    url: URL_MOBILE_APP + '/jadwal-dokter/dokter',
    headers: API_HEADER_WS,
  })
    .then(response => {
      if (response.data.metadata.code != 200) {
        dispatchError(
          dispatch,
          JADWAL_DOKTER_ERROR,
          response.data.metadata.message,
        );
      } else {
        dispatchSuccess(
          dispatch,
          JADWAL_DOKTER_SUCCESS,
          response.data.response,
        );
      }
    })
    .catch(error => {
      dispatchError(dispatch, JADWAL_DOKTER_ERROR, error);
      console.log(error);
      alert(error);
    });
};

export const getJadwalDokterDetail = dokter => async (dispatch, getState) => {
  dispatchLoading(dispatch, JADWAL_DOKTER_DETAIL_LOADING);

  await axios({
    method: 'GET',
    url: URL_MOBILE_APP + '/jadwal/dokter/' + dokter,
    headers: API_HEADER_WS,
  })
    .then(response => {
      if (response.data.metadata.code != 200) {
        dispatchError(
          dispatch,
          JADWAL_DOKTER_DETAIL_ERROR,
          response.data.metadata.message,
        );
      } else {
        dispatchSuccess(
          dispatch,
          JADWAL_DOKTER_DETAIL_SUCCESS,
          response.data.response,
        );
      }
    })
    .catch(error => {
      dispatchError(dispatch, JADWAL_DOKTER_DETAIL_ERROR, error);
      console.log(error);
      alert(error);
    });
};

export const getJadwalPoli = () => async (dispatch, getState) => {
  dispatchLoading(dispatch, JADWAL_POLI_LOADING);

  await axios({
    method: 'GET',
    url: URL_MOBILE_APP + '/jadwal-dokter/poli',
    headers: API_HEADER_WS,
  })
    .then(response => {
      if (response.data.metadata.code != 200) {
        dispatchError(
          dispatch,
          JADWAL_POLI_ERROR,
          response.data.metadata.message,
        );
      } else {
        dispatchSuccess(dispatch, JADWAL_POLI_SUCCESS, response.data.response);
      }
    })
    .catch(error => {
      dispatchError(dispatch, JADWAL_POLI_ERROR, error);
      console.log(error);
      alert(error);
    });
};

export const getJadwalPoliDetail = poli => async (dispatch, getState) => {
  dispatchLoading(dispatch, JADWAL_POLI_DETAIL_LOADING);

  await axios({
    method: 'GET',
    url: URL_MOBILE_APP + '/jadwal-dokter/poli/' + poli,
    headers: API_HEADER_WS,
  })
    .then(response => {

      if (response.data.metadata.code != 200) {
        dispatchError(
          dispatch,
          JADWAL_POLI_DETAIL_ERROR,
          response.data.metadata.message,
        );
      } else {
        dispatchSuccess(
          dispatch,
          JADWAL_POLI_DETAIL_SUCCESS,
          response.data.response,
        );
      }
    })
    .catch(error => {
      dispatchError(dispatch, JADWAL_DOKTER_DETAIL_ERROR, error);
      console.log(error);
      alert(error);
    });
};

export const getKuotaPoli = (poli, tgl) => async (dispatch, getState) => {
  dispatchLoading(dispatch, KUOTA_POLI_LOADING);
  await axios({
    method: 'GET',
    url: URL_MOBILE_APP + '/sisakuota/poli/' + poli + '/tgl/' + tgl,
    headers: API_HEADER_WS,
  })
    .then(response => {
      if (response.data.metadata.code != 200) {
        dispatchError(
          dispatch,
          KUOTA_POLI_ERROR,
          response.data.metadata.message,
        );
      } else {
        dispatchSuccess(dispatch, KUOTA_POLI_SUCCESS, response.data.response);
      }
    })
    .catch(error => {
      dispatchError(dispatch, KUOTA_POLI_ERROR, error);
      console.log(error);
      alert(error);
    });
};
