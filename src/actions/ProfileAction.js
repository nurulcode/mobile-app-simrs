import {UPDATE_PROFILE} from '../constants/UserConstat';
import FIREBASE from '../config/FIREBASE';
import {
  dispatchError,
  dispatchLoading,
  dispatchSuccess,
  storeData,
} from '../utils';



export const updateDataProfile = data => {
  return dispatch => {
    dispatchLoading(dispatch, UPDATE_PROFILE);

    const dataBaru = {
      uid: data.uid,
      nama: data.nama,
      alamat: data.alamat,
      nohp: data.nohp,
      kota: data.kota,
      provinsi: data.provinsi,
      email: data.email,
      status: 'user',
      avatar: data.updateAvatar ? data.avatarForDB : data.avatarLama,
    };

    FIREBASE.database()
      .ref('users/' + data.uid)
      .update(dataBaru)
      .then(res => {
        dispatchSuccess(dispatch, UPDATE_PROFILE, res ? res : []);
        storeData('user', dataBaru);
      })
      .catch(error => {
        dispatchError(dispatch, UPDATE_PROFILE, error.message);

        alert(error.message);
      });
  };
};
