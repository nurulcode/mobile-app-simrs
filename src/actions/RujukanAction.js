import axios from 'axios';
import {
  RUJUKAN_LIST,
  RUJUKAN_LOADING,
  RUJUKAN_SUCCESS,
  RUJUKAN_ERROR,
} from '../constants/RujukanConstat';

import {
  API_HEADER_WS,
  dispatchError,
  dispatchLoading,
  dispatchSuccess,
  storeData,
  URL_VCLAIM,
} from '../utils';

export const cariRujukanVclaim = noRujukan => async (dispatch, getState) => {
  dispatchLoading(dispatch, RUJUKAN_LOADING);

  const {
    UserLoginReducer: {userInfo},
  } = getState();

  const dataLogin = await userInfo.then(data => {
    return data;
  });
  
  await axios({
    method: 'GET',
    url: URL_VCLAIM + '/Rujukan/' + noRujukan,
    headers: API_HEADER_WS,
  })
    .then(response => {
      if (response.data.metaData.code != 200) {
        dispatchError(dispatch, RUJUKAN_ERROR, response.data.metaData.message);
      } else {
        dispatchSuccess(dispatch, RUJUKAN_SUCCESS, response.data.response);
      }
    })
    .catch(error => {
      dispatchError(dispatch, RUJUKAN_ERROR, error);
      console.log(error);
      alert(error);
    });
};

export const cariRencanaKontrolVclaim = noSurkon => async (dispatch, getState) => {
  dispatchLoading(dispatch, RUJUKAN_LOADING);

  const {
    UserLoginReducer: {userInfo},
  } = getState();

  const dataLogin = await userInfo.then(data => {
    return data;
  });
  
  await axios({
    method: 'GET',
    url: URL_VCLAIM + '/RencanaKontrol/noSuratKontrol/' + noSurkon,
    headers: API_HEADER_WS,
  })
    .then(response => {
      if (response.data.metaData.code != 200) {
        dispatchError(dispatch, RUJUKAN_ERROR, response.data.metaData.message);
      } else {
        dispatchSuccess(dispatch, RUJUKAN_SUCCESS, response.data.response);
      }
    })
    .catch(error => {
      dispatchError(dispatch, RUJUKAN_ERROR, error);
      console.log(error);
      alert(error);
    });
};

