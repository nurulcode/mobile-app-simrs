import axios from 'axios';
import {
  AMBIL_ANTREAN_ERROR,
  AMBIL_ANTREAN_LOADING,
  AMBIL_ANTREAN_SUCCESS,
} from '../constants/AntreanConstat';

import {
  API_HEADER_WS,
  dispatchError,
  dispatchLoading,
  dispatchSuccess,
  URL_MOBILE_APP,
} from '../utils';

export const ambilAntreanPoli = data => async (dispatch, getState) => {
  dispatchLoading(dispatch, AMBIL_ANTREAN_LOADING);

  await axios
    .post(URL_MOBILE_APP + '/ambilantrean', data, {
      headers: API_HEADER_WS,
    })
    .then(response => {
      if (response.data.metadata.code != 200) {
        dispatchError(
          dispatch,
          AMBIL_ANTREAN_ERROR,
          response.data.metadata.message,
        );
      } else {
        dispatchSuccess(
          dispatch,
          AMBIL_ANTREAN_SUCCESS,
          response.data.response,
        );
      }
    })
    .catch(error => {
      dispatchError(dispatch, AMBIL_ANTREAN_ERROR, error);
      console.log(error);
      alert(error);
    });
};
export const ambilAntreanPoliNonJkn = data => async (dispatch, getState) => {
  dispatchLoading(dispatch, AMBIL_ANTREAN_LOADING);

  await axios
    .post(URL_MOBILE_APP + '/nonjkn/ambilantrean', data, {
      headers: API_HEADER_WS,
    })
    .then(response => {
      if (response.data.metadata.code != 200) {
        dispatchError(
          dispatch,
          AMBIL_ANTREAN_ERROR,
          response.data.metadata.message,
        );
      } else {
        dispatchSuccess(
          dispatch,
          AMBIL_ANTREAN_SUCCESS,
          response.data.response,
        );
      }
    })
    .catch(error => {
      dispatchError(dispatch, AMBIL_ANTREAN_ERROR, error);
      console.log(error);
      alert(error);
    });
};
