import axios from 'axios';
import {
  BOOKING_LOADING,
  BOOKING_SUCCESS,
  BOOKING_ERROR,
} from '../constants/KunjunganConstat';

import {
  API_HEADER_WS,
  dispatchError,
  dispatchLoading,
  dispatchSuccess,
  storeData,
  URL_MOBILE_APP,
  URL_VCLAIM,
} from '../utils';

export const listBookingAntrean = norm => async (dispatch, getState) => {
  dispatchLoading(dispatch, BOOKING_LOADING);
  await axios({
    method: 'GET',
    url: URL_MOBILE_APP + '/list-booking/' + norm,
    headers: API_HEADER_WS,
  })
    .then(response => {
      if (response.data.metadata.code != 200) {
        dispatchError(dispatch, BOOKING_ERROR, response.data.metadata.message);
      } else {
        dispatchSuccess(dispatch, BOOKING_SUCCESS, response.data.response);
      }
    })
    .catch(error => {
      dispatchError(dispatch, BOOKING_ERROR, error);
      console.log(error);
      alert(error);
    });
};
