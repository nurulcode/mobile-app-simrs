import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import {getData} from './utils';

const userInfoFromStorage = getData('userInfo') ? getData('userInfo') : null;

const initialState = {
  UserLoginReducer: {userInfo: userInfoFromStorage},
};

const store = createStore(rootReducer, initialState, applyMiddleware(thunk));

export default store;
