import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Router from './router';
import {Provider} from 'react-redux';
import store from './store';
// import {enGB, registerTranslation} from 'react-native-paper-dates';
// registerTranslation('en-GB', enGB);
import 'intl';
import 'intl/locale-data/jsonp/id-ID';
// import 'intl/locale-data/jsonp/en';

function App() {
  return (
    // initialRouteName = router yang pertama kali di jalankan
    <Provider store={store}>
      <NavigationContainer initialRouteName="Splash">
        <Router />
      </NavigationContainer>
    </Provider>
  );
}

export default App;
