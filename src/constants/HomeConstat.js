export const DTT_LIST = 'DTT_LIST';
export const DTT_LOADING = 'DTT_LOADING';
export const DTT_SUCCESS = 'DTT_SUCCESS';
export const DTT_ERROR = 'DTT_ERROR';

export const KUOTA_POLI_LIST = 'KUOTA_POLI_LIST';
export const KUOTA_POLI_LOADING = 'KUOTA_POLI_LOADING';
export const KUOTA_POLI_SUCCESS = 'KUOTA_POLI_SUCCESS';
export const KUOTA_POLI_ERROR = 'KUOTA_POLI_ERROR';

export const JADWAL_DOKTER_LIST = 'JADWAL_DOKTER_LIST';
export const JADWAL_DOKTER_LOADING = 'JADWAL_DOKTER_LOADING';
export const JADWAL_DOKTER_SUCCESS = 'JADWAL_DOKTER_SUCCESS';
export const JADWAL_DOKTER_ERROR = 'JADWAL_DOKTER_ERROR';

export const  JADWAL_DOKTER_DETAIL_LIST = ' JADWAL_DOKTER_DETAIL_LIST';
export const  JADWAL_DOKTER_DETAIL_LOADING = ' JADWAL_DOKTER_DETAIL_LOADING';
export const  JADWAL_DOKTER_DETAIL_SUCCESS = ' JADWAL_DOKTER_DETAIL_SUCCESS';
export const  JADWAL_DOKTER_DETAIL_ERROR = ' JADWAL_DOKTER_DETAIL_ERROR';

export const JADWAL_POLI_LIST = 'JADWAL_POLI_LIST';
export const JADWAL_POLI_LOADING = 'JADWAL_POLI_LOADING';
export const JADWAL_POLI_SUCCESS = 'JADWAL_POLI_SUCCESS';
export const JADWAL_POLI_ERROR = 'JADWAL_POLI_ERROR';

export const  JADWAL_POLI_DETAIL_LIST = ' JADWAL_POLI_DETAIL_LIST';
export const  JADWAL_POLI_DETAIL_LOADING = ' JADWAL_POLI_DETAIL_LOADING';
export const  JADWAL_POLI_DETAIL_SUCCESS = ' JADWAL_POLI_DETAIL_SUCCESS';
export const  JADWAL_POLI_DETAIL_ERROR = ' JADWAL_POLI_DETAIL_ERROR';

export const ANTREAN_POLI_LIST = 'ANTREAN_POLI_LIST';
export const ANTREAN_POLI_LOADING = 'ANTREAN_POLI_LOADING';
export const ANTREAN_POLI_SUCCESS = 'ANTREAN_POLI_SUCCESS';
export const ANTREAN_POLI_ERROR = 'ANTREAN_POLI_ERROR';