import {
  PROFILE_ERROR,
  PROFILE_LOADING,
  PROFILE_SUCCESS,
} from '../../constants/AuthConstat';
import {UPDATE_PROFILE} from '../../constants/UserConstat';

const initialState = {
  updateProfileLoading: false,
  updateProfileData: false,
  updateProfileError: false,
};

export const ProfileReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PROFILE:
      return {
        ...state,
        updateProfileLoading: action.payload.loading,
        updateProfileData: action.payload.data,
        updateProfileError: action.payload.errorMessage,
      };
    default:
      return initialState;
  }
};

export const ProfileMobileReducer = (
  state = {
    profileLoading: false,
    profileData: false,
    profileError: false,
  },
  action,
) => {
  switch (action.type) {
    case PROFILE_LOADING:
      return {
        profileLoading: true,
        profileData: false,
        profileError: false,
      };
    case PROFILE_SUCCESS:
      return {
        profileLoading: false,
        profileData: action.payload.data,
        profileError: false,
      };
    case PROFILE_ERROR:
      return {
        profileLoading: false,
        profileData: false,
        profileError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};
