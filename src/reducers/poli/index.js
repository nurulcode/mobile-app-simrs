import {
  POLI_SUCCESS,
  POLI_ERROR,
  POLI_LOADING,
  DOKTER_SUCCESS,
  DOKTER_ERROR,
  DOKTER_LOADING,
} from '../../constants/JadwalDokterPoliConstat';

export const PoliReducer = (
  state = {
    poliLoading: false,
    poliData: false,
    poliError: false,
  },
  action,
) => {
  switch (action.type) {
    case POLI_LOADING:
      return {
        poliLoading: true,
        poliData: false,
        poliError: false,
      };
    case POLI_SUCCESS:
      return {
        poliLoading: false,
        poliData: action.payload.data,
        poliError: false,
      };
    case POLI_ERROR:
      return {
        poliLoading: false,
        poliData: false,
        poliError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};
export const DokterReducer = (
  state = {
    dokterLoading: false,
    dokterData: false,
    dokterError: false,
  },
  action,
) => {
  switch (action.type) {
    case DOKTER_LOADING:
      return {
        dokterLoading: true,
        dokterData: false,
        dokterError: false,
      };
    case DOKTER_SUCCESS:
      return {
        dokterLoading: false,
        dokterData: action.payload.data,
        dokterError: false,
      };
    case DOKTER_ERROR:
      return {
        dokterLoading: false,
        dokterData: false,
        dokterError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};
