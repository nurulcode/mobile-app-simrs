import {
  DTT_SUCCESS,
  DTT_ERROR,
  DTT_LOADING,
  ANTREAN_POLI_LOADING,
  ANTREAN_POLI_SUCCESS,
  ANTREAN_POLI_ERROR,
  JADWAL_DOKTER_LOADING,
  JADWAL_DOKTER_SUCCESS,
  JADWAL_DOKTER_ERROR,
  JADWAL_DOKTER_DETAIL_LOADING,
  JADWAL_DOKTER_DETAIL_SUCCESS,
  JADWAL_DOKTER_DETAIL_ERROR,
  JADWAL_POLI_LOADING,
  JADWAL_POLI_SUCCESS,
  JADWAL_POLI_ERROR,
  JADWAL_POLI_DETAIL_LOADING,
  JADWAL_POLI_DETAIL_SUCCESS,
  JADWAL_POLI_DETAIL_ERROR,
  KUOTA_POLI_LOADING,
  KUOTA_POLI_SUCCESS,
  KUOTA_POLI_ERROR,
} from '../../constants/HomeConstat';

export const DisplayTempatTidurReducer = (
  state = {
    dttLoading: false,
    dttData: false,
    dttError: false,
  },
  action,
) => {
  switch (action.type) {
    case DTT_LOADING:
      return {
        dttLoading: true,
        dttData: false,
        dttError: false,
      };
    case DTT_SUCCESS:
      return {
        dttLoading: false,
        dttData: action.payload.data,
        dttError: false,
      };
    case DTT_ERROR:
      return {
        dttLoading: false,
        dttData: false,
        dttError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};

export const MonitorAntreanPoliReducer = (
  state = {
    antreanPoliLoading: false,
    antreanPoliData: false,
    antreanPoliError: false,
  },
  action,
) => {
  switch (action.type) {
    case ANTREAN_POLI_LOADING:
      return {
        antreanPoliLoading: true,
        antreanPoliData: false,
        antreanPoliError: false,
      };
    case ANTREAN_POLI_SUCCESS:
      return {
        antreanPoliLoading: false,
        antreanPoliData: action.payload.data,
        antreanPoliError: false,
      };
    case ANTREAN_POLI_ERROR:
      return {
        antreanPoliLoading: false,
        antreanPoliData: false,
        antreanPoliError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};

export const DokterJadwalReducer = (
  state = {
    dokterJadwalLoading: false,
    dokterJadwalData: false,
    dokterJadwalError: false,
  },
  action,
) => {
  switch (action.type) {
    case JADWAL_DOKTER_LOADING:
      return {
        dokterJadwalLoading: true,
        dokterJadwalData: false,
        dokterJadwalError: false,
      };
    case JADWAL_DOKTER_SUCCESS:
      return {
        dokterJadwalLoading: false,
        dokterJadwalData: action.payload.data,
        dokterJadwalError: false,
      };
    case JADWAL_DOKTER_ERROR:
      return {
        dokterJadwalLoading: false,
        dokterJadwalData: false,
        dokterJadwalError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};

export const DokterJadwalDetailReducer = (
  state = {
    dokterJadwalDetailLoading: false,
    dokterJadwalDetailData: false,
    dokterJadwalDetailError: false,
  },
  action,
) => {
  switch (action.type) {
    case JADWAL_DOKTER_DETAIL_LOADING:
      return {
        dokterJadwalDetailLoading: true,
        dokterJadwalDetailData: false,
        dokterJadwalDetailError: false,
      };
    case JADWAL_DOKTER_DETAIL_SUCCESS:
      return {
        dokterJadwalDetailLoading: false,
        dokterJadwalDetailData: action.payload.data,
        dokterJadwalDetailError: false,
      };
    case JADWAL_DOKTER_DETAIL_ERROR:
      return {
        dokterJadwalDetailLoading: false,
        dokterJadwalDetailData: false,
        dokterJadwalDetailError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};

export const PoliJadwalReducer = (
  state = {
    poliJadwalLoading: false,
    poliJadwalData: false,
    poliJadwalError: false,
  },
  action,
) => {
  switch (action.type) {
    case JADWAL_POLI_LOADING:
      return {
        poliJadwalLoading: true,
        poliJadwalData: false,
        poliJadwalError: false,
      };
    case JADWAL_POLI_SUCCESS:
      return {
        poliJadwalLoading: false,
        poliJadwalData: action.payload.data,
        poliJadwalError: false,
      };
    case JADWAL_POLI_ERROR:
      return {
        poliJadwalLoading: false,
        poliJadwalData: false,
        poliJadwalError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};

export const PoliJadwalDetailReducer = (
  state = {
    poliJadwalDetailLoading: false,
    poliJadwalDetailData: false,
    poliJadwalDetailError: false,
  },
  action,
) => {
  switch (action.type) {
    case JADWAL_POLI_DETAIL_LOADING:
      return {
        poliJadwalDetailLoading: true,
        poliJadwalDetailData: false,
        poliJadwalDetailError: false,
      };
    case JADWAL_POLI_DETAIL_SUCCESS:
      return {
        poliJadwalDetailLoading: false,
        poliJadwalDetailData: action.payload.data,
        poliJadwalDetailError: false,
      };
    case JADWAL_POLI_DETAIL_ERROR:
      return {
        poliJadwalDetailLoading: false,
        poliJadwalDetailData: false,
        poliJadwalDetailError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};

export const KuotaPoliReducer = (
  state = {
    kuotaPoliLoading: false,
    kuotaPoliData: false,
    kuotaPoliError: false,
  },
  action,
) => {
  switch (action.type) {
    case KUOTA_POLI_LOADING:
      return {
        kuotaPoliLoading: true,
        kuotaPoliData: false,
        kuotaPoliError: false,
      };
    case KUOTA_POLI_SUCCESS:
      return {
        kuotaPoliLoading: false,
        kuotaPoliData: action.payload.data,
        kuotaPoliError: false,
      };
    case KUOTA_POLI_ERROR:
      return {
        kuotaPoliLoading: false,
        kuotaPoliData: false,
        kuotaPoliError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};
