import {
  BOOKING_ERROR,
  BOOKING_LOADING,
  BOOKING_SUCCESS,
} from '../../constants/KunjunganConstat';

export const BookingReducer = (
  state = {
    bookingLoading: false,
    bookingData: false,
    bookingError: false,
  },
  action,
) => {
  switch (action.type) {
    case BOOKING_LOADING:
      return {
        bookingLoading: true,
        bookingData: false,
        bookingError: false,
      };
    case BOOKING_SUCCESS:
      return {
        bookingLoading: false,
        bookingData: action.payload.data,
        bookingError: false,
      };
    case BOOKING_ERROR:
      return {
        bookingLoading: false,
        bookingData: false,
        bookingError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};
