import {
  KERANJANG_DATA,
  KERANJANG_ERROR,
  KERANJANG_LOADING,
  KERANJANG_SUCCESS,
  LIST_KERANJANG_ERROR,
  LIST_KERANJANG_LOADING,
  LIST_KERANJANG_SUCCESS,
  REMOVE_KERANJANG_ERROR,
  REMOVE_KERANJANG_LOADING,
  REMOVE_KERANJANG_SUCCESS,
} from '../../constants/KeranjangConstat';

const initialState = {
  keranjangLoading: false,
  keranjangData: false,
  keranjangError: false,
  keranjangStatus: false,
};

export const KeranjangReducer = (state = initialState, action) => {
  switch (action.type) {
    case KERANJANG_LOADING:
      return {
        keranjangLoading: true,
        keranjangData: false,
        keranjangError: false,
        keranjangStatus: false,
      };
    case KERANJANG_SUCCESS:
      return {
        keranjangLoading: false,
        keranjangData: false,
        keranjangError: false,
        keranjangStatus: true,
      };
    case KERANJANG_DATA:
      return {
        keranjangLoading: false,
        keranjangData: action.payload.data,
        keranjangError: false,
        keranjangStatus: false,
      };
    case KERANJANG_ERROR:
      return {
        keranjangLoading: false,
        keranjangData: false,
        keranjangError: action.payload.errorMessage,
        keranjangStatus: false,
      };
    default:
      return state;
  }
};
export const ListKeranjangReducer = (state = {}, action) => {
  switch (action.type) {
    case LIST_KERANJANG_LOADING:
      return {
        listKeranjangLoading: true,
        listKeranjangData: false,
        listKeranjangError: false,
        listKeranjangStatus: false,
      };
    case LIST_KERANJANG_SUCCESS:
      return {
        listKeranjangLoading: false,
        listKeranjangData: action.payload.data,
        listKeranjangError: false,
        listKeranjangStatus: true,
      };
    case LIST_KERANJANG_ERROR:
      return {
        listKeranjangLoading: false,
        listKeranjangData: false,
        listKeranjangError: action.payload.errorMessage,
        listKeranjangStatus: false,
      };
    default:
      return state;
  }
};

export const removeKeranjangReducer = (state = {}, action) => {
  switch (action.type) {
    case REMOVE_KERANJANG_LOADING:
      return {
        removeKeranjangLoading: true,
        removeKeranjangData: false,
        removeKeranjangError: false,
        removeKeranjangStatus: false,
      };
    case REMOVE_KERANJANG_SUCCESS:
      return {
        removeKeranjangLoading: false,
        removeKeranjangData: action.payload.data,
        removeKeranjangError: false,
        removeKeranjangStatus: true,
      };
    case REMOVE_KERANJANG_ERROR:
      return {
        removeKeranjangLoading: false,
        removeKeranjangData: false,
        removeKeranjangError: action.payload.errorMessage,
        removeKeranjangStatus: false,
      };
    default:
      return state;
  }
};
