import {
  GET_USER,
  USER_LOGIN_ERROR,
  USER_LOGIN_LOADING,
  USER_LOGIN_SUCCESS,
} from '../../constants/UserConstat';

export const UserReducer = (state = {dataUser: false}, action) => {
  switch (action.type) {
    case GET_USER:
      return {
        user: action.payload,
      };
    default:
      return state;
  }
};

export const UserLoginReducer = (state = {}, action) => {
  switch (action.type) {
    case USER_LOGIN_LOADING:
      return {loading: true};
    case USER_LOGIN_SUCCESS:
      return {loading: false, userInfo: action.payload};
    case USER_LOGIN_ERROR:
      return {loading: false, error: action.payload};
    default:
      return state;
  }
};

// const initialState = {
//   dataUser: false,
// };

// export default function (state = initialState, action) {
//   switch (action.type) {
//     case GET_USER:
//       return {
//         ...state,
//         dataUser: action.payload,
//       };
//     default:
//       return false;
//   }
// }
