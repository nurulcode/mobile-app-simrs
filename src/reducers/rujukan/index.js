import {
  RUJUKAN_LIST,
  RUJUKAN_SUCCESS,
  RUJUKAN_ERROR,
  RUJUKAN_LOADING,
} from '../../constants/RujukanConstat';

const initialState = {
  rujukanLoading: false,
  rujukanData: false,
  rujukanError: false,
};

export const RujukanReducer = (state = initialState, action) => {
  switch (action.type) {
    case RUJUKAN_LOADING:
      return {
        rujukanLoading: true,
        rujukanData: false,
        rujukanError: false,
      };
    case RUJUKAN_SUCCESS:
      return {
        rujukanLoading: false,
        rujukanData: action.payload.data,
        rujukanError: false,
      };
    case RUJUKAN_ERROR:
      return {
        rujukanLoading: false,
        rujukanData: false,
        rujukanError: action.payload.errorMessage,
      };
    default:
      return initialState;
  }
};