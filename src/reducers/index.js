import {combineReducers} from 'redux';
import {UserReducer, UserLoginReducer} from './user';
import {RajaOngkirReducer} from './rajaongkir';
import {AuthReducer, LoginReducer} from './auth';
import {ProfileReducer, ProfileMobileReducer} from './profile';
import {LigaReducer, LigaByIdReducer} from './liga';
import {JerseyReducer, JerseyByLigaReducer} from './jersey';
import {
  KeranjangReducer,
  ListKeranjangReducer,
  removeKeranjangReducer,
} from './keranjang';
import {RujukanReducer} from './rujukan';
import {PoliReducer, DokterReducer} from './poli';
import {AmbilAntreanReducer} from './antrean';
import {BookingReducer} from './kunjungan';
import {
  DisplayTempatTidurReducer,
  MonitorAntreanPoliReducer,
  DokterJadwalDetailReducer,
  DokterJadwalReducer,
  PoliJadwalDetailReducer,
  PoliJadwalReducer,
  KuotaPoliReducer,
} from './home';

const rootReducer = combineReducers({
  UserReducer,
  UserLoginReducer,
  RajaOngkirReducer,
  AuthReducer,
  LoginReducer,
  ProfileReducer,
  ProfileMobileReducer,
  LigaReducer,
  JerseyReducer,
  JerseyByLigaReducer,
  LigaByIdReducer,
  KeranjangReducer,
  ListKeranjangReducer,
  removeKeranjangReducer,

  RujukanReducer,
  PoliReducer,
  DokterReducer,

  AmbilAntreanReducer,

  DisplayTempatTidurReducer,
  MonitorAntreanPoliReducer,
  MonitorAntreanPoliReducer,
  DokterJadwalReducer,
  DokterJadwalDetailReducer,
  DokterJadwalReducer,
  DokterJadwalDetailReducer,
  PoliJadwalReducer,
  PoliJadwalDetailReducer,
  KuotaPoliReducer,

  BookingReducer,
});

export default rootReducer;
