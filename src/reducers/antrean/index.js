import {
  AMBIL_ANTREAN_ERROR,
  AMBIL_ANTREAN_LOADING,
  AMBIL_ANTREAN_RESET,
  AMBIL_ANTREAN_SUCCESS,
} from '../../constants/AntreanConstat';

export const AmbilAntreanReducer = (
  state = {
    ambilAntreanLoading: false,
    ambilAntreanData: false,
    ambilAntreanError: false,
  },
  action,
) => {
  switch (action.type) {
    case AMBIL_ANTREAN_LOADING:
      return {
        ambilAntreanLoading: true,
        ambilAntreanData: false,
        ambilAntreanError: false,
      };
    case AMBIL_ANTREAN_SUCCESS:
      return {
        ambilAntreanLoading: false,
        ambilAntreanData: action.payload.data,
        ambilAntreanError: false,
      };
    case AMBIL_ANTREAN_ERROR:
      return {
        ambilAntreanLoading: false,
        ambilAntreanData: false,
        ambilAntreanError: action.payload.errorMessage,
      };
    case AMBIL_ANTREAN_RESET:
      return {
        ambilAntreanLoading: false,
        ambilAntreanData: false,
        ambilAntreanError: false,
      };
    default:
      return state;
  }
};
