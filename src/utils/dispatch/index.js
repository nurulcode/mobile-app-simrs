export const dispatchLoading = (dispatch, type) => {
    dispatch({
        type: type,
        payload: {
          loading: true,
          status : false,
          data: false,
          errorMessage: false,
        },
      });
}

export const dispatchSuccess = (dispatch, type, data) => {
    dispatch({
        type: type,
        payload: {
          loading: false,
          status : true,
          data: data,
          errorMessage: false,
        },
      });
}

export const dispatchData = (dispatch, type, data) => {
    dispatch({
        type: type,
        payload: {
          loading: false,
          status : false,
          data: data,
          errorMessage: false,
        },
      });
}
export const dispatchError = (dispatch, type, error) => {
    dispatch({
        type: type,
        payload: {
          loading: false,
          status : false,
          data: false,
          errorMessage: error,
        },
      });
}