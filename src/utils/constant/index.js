export const heightMobileUI = 896;
export const widthMobileUI = 414;

export const API_KEY = 'ef806c08aba630fc70fb640f10cfec5c';
export const API_RAJAONGKIR = 'https://api.rajaongkir.com/starter/';
export const API_HEADER_RAJAONGKIR = {
  key: API_KEY,
};
export const API_TIMEOUT = 120000;

export const URL = 'https://7ced-180-249-154-5.ap.ngrok.io';
export const URL_VCLAIM = URL + '/api/jkn/v2';
export const URL_MOBILE_APP = URL + '/api/mobileapp';
export const AUTH_WS = 'gn8Vh6SZWbgkIG8fRtixEJB7hXaS06ZuGRDmnIUGrkE';

export const API_HEADER_WS = {
  'Content-Type': 'application/json',
  'x-secretkey': AUTH_WS,
};
