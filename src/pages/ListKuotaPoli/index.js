import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Alert,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {useDispatch, useSelector} from 'react-redux';
import {getKuotaPoli} from '../../actions/HomeAction';
import {Bed2, HealthCare, IconArrowRight, IndonesiaRupiah} from '../../assets';
import {Gap} from '../../components/atoms';
import {ListHistory} from '../../components/molecules';
import {KUOTA_POLI_ERROR} from '../../constants/HomeConstat';
import {dummyJadwalDokter, dummyKuotaPoli} from '../../data';
import {
  colors,
  dispatchError,
  fonts,
  heightMobileUI,
  responsiveHeight,
  responsiveWidth,
} from '../../utils';

const ListKuotaPoli = props => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const params = props.route.params;

  const KuotaPoliRed = useSelector(state => state.KuotaPoliReducer);
  const {kuotaPoliData, kuotaPoliError, kuotaPoliLoading} = KuotaPoliRed;

  useEffect(() => {
    if (kuotaPoliError) {
      Alert.alert('Error', kuotaPoliError);
      navigation.goBack();
    } else {
      dispatch(getKuotaPoli(params.poli, params.tgl));
    }
  }, [dispatch, kuotaPoliError]);

  return (
    <View style={styles.pages}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.wrap}>
          <View style={styles.container}>
            <View style={styles.card}>
              <Text style={styles.textDate}>{kuotaPoliData.poli}</Text>
              <Text style={styles.textDate}>
                {kuotaPoliData.tanggal_periksa}
              </Text>
            </View>
          </View>
          {kuotaPoliData ? (
            kuotaPoliData.list.map((key, index) => (
              <View key={index}>
                <View style={styles.container}>
                  <View style={styles.card}>
                    <Text style={styles.textTitle}>{key.nama}</Text>
                    <Gap height={7} />
                    <View style={styles.display}>
                      <Text style={styles.text}>Hari</Text>
                      <Text style={styles.text}>{key.hari}</Text>
                    </View>
                    <View style={styles.display}>
                      <Text style={styles.text}>Kuota</Text>
                      <Text style={styles.text}>{key.kuota}</Text>
                    </View>
                    <View style={styles.display}>
                      <Text style={styles.text}>Sisa Kuota</Text>
                      <Text style={styles.text}>{key.sisa_kuota}</Text>
                    </View>
                    <View style={styles.display}>
                      <Text style={styles.text}>Terdaftar</Text>
                      <Text style={styles.text}>{key.terdaftar}</Text>
                    </View>
                  </View>
                </View>
              </View>
            ))
          ) : kuotaPoliLoading ? (
            <View
              style={{
                marginTop: responsiveHeight(40),
              }}>
              <ActivityIndicator color={colors.primary} />
            </View>
          ) : (
            <View
              style={{
                marginTop: responsiveHeight(40),
                alignItems: 'center',
              }}>
              <Text>List Data Kosong</Text>
            </View>
          )}
        </View>
        <Gap height={20} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    paddingHorizontal: responsiveWidth(20),
    flex: 1,
  },
  pages: {
    backgroundColor: colors.white,
    flex: 1,
  },
  wrap: {
    marginHorizontal: 30,
  },
  container: {
    flexDirection: 'row',
    backgroundColor: colors.white,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    padding: responsiveHeight(10),
    borderRadius: 10,
    alignItems: 'center',
    borderEndWidth: 7,
    borderRightColor: colors.secondary,
  },
  text: {
    fontSize: 16,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  textTitle: {
    fontSize: RFValue(18, heightMobileUI),
    fontFamily: fonts.primary.regular,
    color: colors.primary,
    textAlign: 'center',
  },
  textDate: {
    fontSize: RFValue(18, heightMobileUI),
    fontFamily: fonts.primary.bold,
    color: colors.primary,
    textAlign: 'center',
  },
  display: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default ListKuotaPoli;
