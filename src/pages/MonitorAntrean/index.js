import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  useWindowDimensions,
  ActivityIndicator,
} from 'react-native';
import {colors, fonts, heightMobileUI, responsiveHeight} from '../../utils';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import {RFValue} from 'react-native-responsive-fontsize';
import {dummyAntrean} from '../../data';
import {useDispatch, useSelector} from 'react-redux';
import {getAntreanPoli} from '../../actions/HomeAction';
import {useEffect} from 'react';

const FirstRoute = () => {
  const dispatch = useDispatch();

  const MonitorAntreanPoliRed = useSelector(
    state => state.MonitorAntreanPoliReducer,
  );
  const {antreanPoliData, antreanPoliError, antreanPoliLoading} =
    MonitorAntreanPoliRed;

  useEffect(() => {
    dispatch(getAntreanPoli());
  }, []);

  const [data, setData] = useState(dummyAntrean);

  return (
    <View style={styles.pages}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.wrap}>
          {antreanPoliData.length ? (
            antreanPoliData.map((data, index) => (
              <View style={styles.cardContainer} key={index}>
                <View>
                  <Text style={styles.textPoli}>{data.poli}</Text>
                  <Text style={styles.textPoli}>{data.dokter}</Text>
                </View>
                <View style={styles.container}>
                  <View style={styles.card}>
                    <View style={styles.infoAntrean}>
                      <Text style={styles.text}>Total Antrean</Text>
                      <Text style={styles.text}>{data.total_antrean}</Text>
                    </View>
                    <View
                      style={{
                        borderBottomColor: 'black',
                        borderBottomWidth: StyleSheet.hairlineWidth,
                        marginVertical: 3,
                      }}
                    />
                    <View style={styles.infoAntrean}>
                      <Text style={styles.text}>Sisa Antrean</Text>
                      <Text style={styles.text}>{data.sisa_antrean}</Text>
                    </View>
                  </View>
                  <View>
                    <View style={styles.antrean}>
                      <Text style={styles.textAntreanTitle}>Antrean</Text>
                      <Text style={styles.textAntrean}>
                        {data.antrean_terakhir}
                      </Text>
                    </View>
                    <View style={styles.panggail}>
                      <Text style={styles.textAntreanTitle}>Panggil</Text>
                      <Text style={styles.textAntrean}>{data.dipanggil}</Text>
                    </View>
                  </View>
                </View>
              </View>
            ))
          ) : antreanPoliLoading ? (
            <View
              style={{
                marginTop: responsiveHeight(40),
              }}>
              <ActivityIndicator color={colors.primary} />
            </View>
          ) : (
            <View
              style={{
                marginTop: responsiveHeight(40),
                alignItems: 'center',
              }}>
              <Text>List Data Kosong</Text>
            </View>
          )}
        </View>
      </ScrollView>
    </View>
  );
};

const SecondRoute = () => (
  <View style={styles.pages}>
    <View
      style={[
        styles.wrap,
        {justifyContent: 'center', alignItems: 'center', flex: 1},
      ]}>
      <Text style={[styles.textHeader, {textAlign: 'center'}]}>
        MASIH DALAM TAHAP PENGEMBANGAN
      </Text>
    </View>
  </View>
);

const renderScene = SceneMap({
  first: FirstRoute,
  second: SecondRoute,
});

const MonitorAntrean = () => {
  const layout = useWindowDimensions();

  const [index, setIndex] = useState(0);
  const [routes] = useState([
    {key: 'first', title: 'Poliklinik'},
    {key: 'second', title: 'Farmasi'},
  ]);

  return (
    <TabView
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{width: layout.width}}
      renderTabBar={props => (
        <TabBar
          {...props}
          style={{backgroundColor: colors.secondary}}
          indicatorStyle={{backgroundColor: colors.primary}}
          pressColor={colors.primary}
        />
      )}
    />
  );
};

const styles = StyleSheet.create({
  pages: {
    backgroundColor: colors.white,
    flex: 1,
  },

  wrap: {
    marginHorizontal: 30,
    marginVertical: 20,
  },

  cardContainer: {
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    borderRadius: 10,
    backgroundColor: colors.white,
    borderRightColor: colors.primary,
    borderRightWidth: 7,
    padding: responsiveHeight(10),
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    borderRadius: 10,
    padding: responsiveHeight(10),
    alignItems: 'center',
    borderColor: colors.secondary,
    borderRightWidth: 7,
    borderWidth: 1,
  },
  card: {
    flex: 1,
    paddingHorizontal: 20,
  },
  infoAntrean: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  antrean: {
    backgroundColor: colors.primary,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  panggail: {
    backgroundColor: colors.primary,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 2,
    borderRadius: 5,
  },
  textAntreanTitle: {
    fontSize: 14,
    fontFamily: fonts.primary.regular,
    color: colors.white,
  },
  textAntrean: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: colors.white,
    marginTop: 5,
  },
  textPoli: {
    fontSize: RFValue(16, heightMobileUI),
    fontFamily: fonts.primary.regular,
    color: colors.primary,
  },
  text: {
    fontSize: 16,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  titleHeader: {
    marginBottom: 10,
  },
  textHeader: {
    fontSize: 20,
    fontFamily: fonts.primary.regular,
    alignSelf: 'center',
    color: colors.primary,
  },
});

export default MonitorAntrean;
