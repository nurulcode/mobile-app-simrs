import React, {useEffect, useState} from 'react';
import {View, StyleSheet, ScrollView, Image, Alert} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Inputan, Pilihan, Tombol} from '../../components/atoms';
import {
  colors,
  dispatchError,
  dispatchSuccess,
  fonts,
  responsiveHeight,
  responsiveWidth,
} from '../../utils';
import {Calendar} from 'react-native-calendars';
import {
  getDokterPoli,
  getPoliVclaim,
} from '../../actions/JadwalDokterPoliAction';
import {ambilAntreanPoli} from '../../actions/AntreanAction';
import {useNavigation} from '@react-navigation/native';
import {
  AMBIL_ANTREAN_ERROR,
  AMBIL_ANTREAN_SUCCESS,
} from '../../constants/AntreanConstat';
import { DateTime } from 'luxon';

const DaftarPasienJkn = props => {
  const navigation = useNavigation();

  const params = props.route.params;
  const [open, setOpen] = useState(false);

  const [poli, setPoli] = useState('');
  const [dokter, setDokter] = useState('');
  const [nama, setNama] = useState(params.user.pasien.nama);

  const [nomorkartu, setNomorkartu] = useState(params.user.pasien.no_bpjs);
  const [nik, setNik] = useState('');
  const [nohp, setNohp] = useState('');
  const [kodepoli, setKodepoli] = useState('');
  const [norm, setNorm] = useState(params.user.pasien.no_rekam_medis);
  const [tanggalperiksa, setTanggalperiksa] = useState(params.tgl);
  const [kodedokter, setKodedokter] = useState('');
  const [jampraktek, setJampraktek] = useState('');
  const [jeniskunjungan, setJeniskunjungan] = useState(
    params.jnsKunjungan.substr(0, 1),
  );
  const [nomorreferensi, setNomorreferensi] = useState(params.noRef);

  const setValueDokter = dokter => {
    setDokter(dokter);
    setKodepoli(dokter.kode_poli_vclaim);
    setKodedokter(dokter.kode_dokter_vclaim);
    const jam = dokter.mulai.substr(0, 5) + '-' + dokter.selesai.substr(0, 5);
    setJampraktek(jam);
  };

  const dispatch = useDispatch();

  const poliRed = useSelector(state => state.PoliReducer);
  const {poliData, poliError, poliLoading} = poliRed;

  const dokterRed = useSelector(state => state.DokterReducer);
  const {dokterData, dokterError, dokterLoading} = dokterRed;

  const ambilAntreanRed = useSelector(state => state.AmbilAntreanReducer);
  const {ambilAntreanData, ambilAntreanError, ambilAntreanLoading} =
    ambilAntreanRed;

  const onSubmit = () => {
    if (
      nomorkartu &&
      nik &&
      nomorreferensi &&
      nohp &&
      norm &&
      kodepoli &&
      kodedokter &&
      tanggalperiksa &&
      jampraktek &&
      jeniskunjungan
    ) {
      const data = {
        nomorkartu,
        nik,
        nomorreferensi,
        nohp,
        norm,
        kodepoli,
        kodedokter,
        tanggalperiksa,
        jampraktek,
        jeniskunjungan,
      };

      dispatch(ambilAntreanPoli(data));
    } else {
      Alert.alert('Error', 'Data tidak boleh kosong');
    }
  };

  const changeDokter = poli => {
    setPoli(poli);

    if (poli && tanggalperiksa) {
      dispatch(getDokterPoli(poli, tanggalperiksa));
    } else {
      Alert.alert('Error', 'Tgl, Poli tidak boleh kosong');
    }
  };
  const changeDate = tanggalperiksa => {
    if (tanggalperiksa) {
      setTanggalperiksa(tanggalperiksa);
      dispatch(getPoliVclaim(tanggalperiksa));
    } else {
      Alert.alert('Error', 'Tgl, Poli tidak boleh kosong');
    }
  };

  useEffect(() => {
    if (ambilAntreanError) {
      Alert.alert('Error', ambilAntreanError);
      dispatchError(dispatch, AMBIL_ANTREAN_ERROR, false);
    }

    if (ambilAntreanData) {
      Alert.alert(
        'Success',
        'Nomor Antrean ' +
          ambilAntreanData.nomorantrean +
          ' ' +
          ambilAntreanData.keterangan,
      );
      dispatchSuccess(dispatch, AMBIL_ANTREAN_SUCCESS, false);
      navigation.replace('MainApp');
    }
  }, [dispatch, ambilAntreanError, ambilAntreanData]);

  return (
    <View style={styles.pages}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Inputan
          label="Nama Peserta"
          value={nama}
          onChangeText={nama => setNama(nama)}
          placeholder="No Rujukan / No Kontrol Pasien"
          disabled
        />

        <Inputan
          label="No Ref / No Rujukan / No Kontrol"
          value={nomorreferensi}
          onChangeText={nomorreferensi => setNomorreferensi(nomorreferensi)}
          placeholder="No Rujukan / No Kontrol Pasien"
          disabled
        />

        <Inputan
          label="Jenis Kunjungan"
          value={jeniskunjungan}
          onChangeText={jeniskunjungan => setJeniskunjungan(jeniskunjungan)}
          placeholder="011223"
          disabled
        />

        <Inputan
          label="No Rekam Medis"
          value={norm}
          onChangeText={norm => setNorm(norm)}
          placeholder="011223"
        />

        <Inputan
          label="No Kartu"
          value={nomorkartu}
          onChangeText={nomorkartu => setNomorkartu(nomorkartu)}
          placeholder="No Rujukan / No Kontrol Pasien"
        />

        <Inputan
          label="NIK"
          value={nik}
          onChangeText={nik => setNik(nik)}
          placeholder="Np Induk Kependudukan"
          keyboardType="number-pad"
        />

        <Inputan
          label="No Hp"
          value={nohp}
          onChangeText={nohp => setNohp(nohp)}
          placeholder="Telepon Max 12 Digit"
          keyboardType="number-pad"
        />

        {jeniskunjungan == '3 (Kontrol)' ? (
          <Inputan
            label="Tgl Kunjungan"
            value={tanggalperiksa}
            disabled
          />
        ) : (
          <Inputan
            label="Tgl Rancana Kunjungan"
            placeholder="yyyy-mm-dd"
            value={tanggalperiksa}
            onTouchStart={() => setOpen(!open)}
            showSoftInputOnFocus={false}
          />
        )}

        {open && (
          <Calendar
            onDayPress={day => {
              changeDate(day.dateString);
              setPoli('');
              setDokter('');
              setOpen(!open);
            }}
            minDate={DateTime.now().plus({days: 1}).toFormat('yyyy-MM-dd')}
            maxDate={DateTime.now().plus({days: 6}).toFormat('yyyy-MM-dd')}
          />
        )}

        <Pilihan
          label="Poliklinik"
          datas={poliData ? poliData : []}
          selectedValue={poli}
          onValueChange={poli_id => changeDokter(poli_id)}
        />

        <Pilihan
          label="Dokter"
          datas={dokterData ? dokterData : []}
          selectedValue={dokter}
          onValueChange={dokter => setValueDokter(dokter)}
        />

        <View style={styles.submit}>
          <Tombol
            loading={ambilAntreanLoading}
            title="Daftar"
            type="text"
            padding={responsiveHeight(15)}
            fontSize={18}
            onPress={() => onSubmit()}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    paddingHorizontal: 30,
    backgroundColor: colors.white,
    paddingTop: 10,
  },

  inputFoto: {
    marginTop: 20,
  },
  label: {
    fontSize: 18,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  foto: {
    width: responsiveWidth(170),
    height: responsiveHeight(200),
    borderRadius: 40,
  },

  wrapperUpload: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },

  tombolChangePhoto: {
    marginLeft: 20,
    flex: 1,
  },
  submit: {
    marginVertical: 10,
  },
});

export default DaftarPasienJkn;
