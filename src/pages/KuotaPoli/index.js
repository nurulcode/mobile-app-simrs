import {useNavigation} from '@react-navigation/native';
import React, {useCallback, useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Image,
  Button,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  Text,
} from 'react-native';
import {Calendar} from 'react-native-calendars';
import {useDispatch, useSelector} from 'react-redux';
import {getPoliVclaim} from '../../actions/JadwalDokterPoliAction';
import {Gap, Inputan, Pilihan, Tombol} from '../../components/atoms';
import {KUOTA_POLI_ERROR} from '../../constants/HomeConstat';
import {
  colors,
  dispatchError,
  fonts,
  responsiveHeight,
  responsiveWidth,
} from '../../utils';
import {DateTime} from 'luxon';

const KuotaPoli = () => {
  const navigation = useNavigation();
  const [poli, setPoli] = useState('');
  const [tgl, setTgl] = useState('');
  const [dateTime, setDateTime] = useState();
  const [open, setOpen] = useState(false);

  // console.log('====================================');
  // console.log(Math.round((new Date()).getTime()));
  // console.log('====================================');

  const dispatch = useDispatch();
  const poliRed = useSelector(state => state.PoliReducer);
  const {poliData, poliError, poliLoading} = poliRed;

  const changeDate = tglK => {
    if (tglK) {
      dispatch(getPoliVclaim(tglK));
    } else {
      Alert.alert('Error', 'Tgl, tidak boleh kosong');
    }
  };

  const onSubmit = () => {
    if (poli && tgl && !poliError) {
      dispatchError(dispatch, KUOTA_POLI_ERROR, false);
      navigation.navigate('ListKuotaPoli', {
        poli,
        tgl,
      });
    } else {
      Alert.alert('Error', 'Tgl dan Poli tidak boleh kosong');
    }
  };

  return (
    <View style={styles.pages}>
      <View>
        <Inputan
          label='Tgl Rencana Kunjungan'
          placeholder="yyyy-mm-dd"
          value={tgl}
          onTouchStart={() => setOpen(!open)}
          showSoftInputOnFocus={false}
        />

        {open && (
          <Calendar
            onDayPress={day => {
              changeDate(day.dateString);
              setPoli('');
              setTgl(day.dateString);
              setOpen(!open);
            }}
            minDate={DateTime.now().plus({days: 1}).toFormat('yyyy-MM-dd')}
          />
        )}

        {poliData && tgl ? (
          <Pilihan
            label="Poliklinik"
            datas={poliData ? poliData : []}
            selectedValue={poli}
            onValueChange={poli => setPoli(poli)}
          />
        ) : poliLoading ? (
          <View
            style={{
              marginTop: responsiveHeight(40),
            }}>
            <ActivityIndicator color={colors.primary} />
          </View>
        ) : tgl ? (
          <View
            style={{
              marginTop: responsiveHeight(40),
              alignItems: 'center',
            }}>
            <Text>Tidak poli buka pada tanggal {tgl}</Text>
          </View>
        ) : (
          <Text></Text>
        )}
      </View>

      <View style={styles.submit}>
        <Tombol
          title="Kuota Poli"
          type="text"
          padding={responsiveHeight(15)}
          fontSize={18}
          onPress={() => onSubmit()}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    paddingHorizontal: 30,
    backgroundColor: colors.white,
    paddingTop: 10,
    justifyContent: 'space-between',
  },
  label: {
    fontSize: 18,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  submit: {
    marginVertical: 10,
  },
});

export default KuotaPoli;
