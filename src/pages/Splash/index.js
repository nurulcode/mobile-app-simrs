import React, {useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import LogoNew from '../../assets/images/logoNew.svg';
import { colors } from '../../utils';
const Splash = props => {
  function redirectHome() {
    setTimeout(() => {
      return props.navigation.replace('MainApp');
    }, 3000);
  }

  useEffect(() => {
    redirectHome();
  }, []);

  return (
    <View style={styles.pages}>
      <LogoNew />
      <View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.white,
  },
});

export default Splash;
