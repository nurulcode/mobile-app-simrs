import Home from './Home';
import Splash from './Splash';
import Profile from './Profile';
import ListJersey from './ListJersey';
import JerseyDetail from './JerseyDetail';
import Keranjang from './Keranjang';
import Checkout from './Checkout';
import EditProfile from './EditProfile';
import ChangePassword from './ChangePassword';
import History from './History';
import Login from './Login';
import Register1 from './Register1';
import Register2 from './Register2';
import DisplayTempatTidur from './DisplayTempatTidur';
import MonitorAntrean from './MonitorAntrean';
import JadwalDokter from './JadwalDokter';
import KuotaPoli from './KuotaPoli';
import FasilitasPelayanan from './FasilitasPelayanan';
import KontakKami from './KontakKami';
import DaftarPasienJkn from './DaftarPasienJkn';
import DaftarPasienNonJkn from './DaftarPasienNonJkn';
import NoRujukanPeserta from './NoRujukanPeserta';
import PembatalanAntrean from './PembatalanAntrean';

export {
  Home,
  Splash,
  Profile,
  ListJersey,
  JerseyDetail,
  Keranjang,
  Checkout,
  EditProfile,
  ChangePassword,
  History,
  Login,
  Register1,
  Register2,
  DisplayTempatTidur,
  MonitorAntrean,
  JadwalDokter,
  KuotaPoli,
  FasilitasPelayanan,
  KontakKami,
  DaftarPasienJkn,
  DaftarPasienNonJkn,
  NoRujukanPeserta,
  PembatalanAntrean,
};
