import React, {useState} from 'react';
import {View, StyleSheet, ScrollView, Image, Alert} from 'react-native';
import {Inputan, Tombol} from '../../components/atoms';
import {colors, fonts, responsiveHeight, responsiveWidth} from '../../utils';

const PembatalanAntrean = props => {
  const params = props.route.params;
  const [noboking, setNobooking] = useState(params.no_booking);
  const [alasan, setAlasan] = useState('');

  const onSubmit = () => {
    if (noboking && alasan) {
      Alert.alert('Success', `Batal Antrean ${noboking} ${alasan}`);
    } else {
      Alert.alert('Error', 'No Booking dan Alasan tidak boleh kosong.');
    }
  };

  return (
    <View style={styles.pages}>
      <View>
        <Inputan label="Kode Booking" value={noboking} />
        <Inputan
          label="Alasan Pembatalan"
          value={alasan}
          onChangeText={alasan => setAlasan(alasan)}
          placeholder="Ada kebutuhan mendadak"
        />
      </View>

      <View style={styles.submit}>
        <Tombol
          color={colors.red}
          title="Batal Antrean"
          type="text"
          icon="submit"
          padding={responsiveHeight(15)}
          fontSize={18}
          onPress={() => onSubmit()}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    paddingHorizontal: 30,
    backgroundColor: colors.white,
    paddingTop: 10,
    justifyContent: 'space-between',
  },
  label: {
    fontSize: 18,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  submit: {
    marginVertical: 10,
  },
});

export default PembatalanAntrean;
