import React, {useEffect, useState} from 'react';
import {View, StyleSheet, ScrollView, Image, Alert} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Inputan, Pilihan, Tombol} from '../../components/atoms';
import {
  colors,
  dispatchError,
  dispatchSuccess,
  fonts,
  getData,
  responsiveHeight,
  responsiveWidth,
} from '../../utils';
import {Calendar} from 'react-native-calendars';
import {
  getDokterPoli,
  getPoliVclaim,
} from '../../actions/JadwalDokterPoliAction';
import {
  ambilAntreanPoli,
  ambilAntreanPoliNonJkn,
} from '../../actions/AntreanAction';
import {useNavigation} from '@react-navigation/native';
import {
  AMBIL_ANTREAN_ERROR,
  AMBIL_ANTREAN_SUCCESS,
} from '../../constants/AntreanConstat';
import {DateTime} from 'luxon';

const DaftarPasienNonJkn = props => {
  const navigation = useNavigation();
  const [open, setOpen] = useState(false);

  const [poli, setPoli] = useState('');
  const [dokter, setDokter] = useState('');
  const [nama, setNama] = useState();
  // const [nama, setNama] = useState(params.user.pasien.nama);

  const [nik, setNik] = useState('');
  const [nohp, setNohp] = useState('');
  const [kodepoli, setKodepoli] = useState('');
  // const [norm, setNorm] = useState(params.no_rekam_medis.pasien.no_rekam_medis);
  const [norm, setNorm] = useState('');
  const [tanggalperiksa, setTanggalperiksa] = useState('');
  const [kodedokter, setKodedokter] = useState('');
  const [jampraktek, setJampraktek] = useState('');
  const [jeniskunjungan, setJeniskunjungan] = useState('');
  // const [jeniskunjungan, setJeniskunjungan] = useState(
  //   params.jnsKunjungan.substr(0, 1),
  // );

  const setValueDokter = dokter => {
    setDokter(dokter);
    setKodepoli(dokter.kode_poli_vclaim);
    setKodedokter(dokter.kode_dokter_vclaim);
    const jam = dokter.mulai.substr(0, 5) + '-' + dokter.selesai.substr(0, 5);
    setJampraktek(jam);
  };

  const dispatch = useDispatch();

  const poliRed = useSelector(state => state.PoliReducer);
  const {poliData, poliError, poliLoading} = poliRed;

  const dokterRed = useSelector(state => state.DokterReducer);
  const {dokterData, dokterError, dokterLoading} = dokterRed;

  const ambilAntreanRed = useSelector(state => state.AmbilAntreanReducer);
  const {ambilAntreanData, ambilAntreanError, ambilAntreanLoading} =
    ambilAntreanRed;

  const onSubmit = () => {
    if (
      nik &&
      nohp &&
      norm &&
      kodepoli &&
      kodedokter &&
      tanggalperiksa &&
      jampraktek &&
      jeniskunjungan
    ) {
      const data = {
        nik,
        nohp,
        norm,
        kodepoli,
        kodedokter,
        tanggalperiksa,
        jampraktek,
        jeniskunjungan,
      };

      dispatch(ambilAntreanPoliNonJkn(data));
    } else {
      Alert.alert('Error', 'Data tidak boleh kosong');
    }
  };

  const changeDokter = poli => {
    setPoli(poli);

    if (poli && tanggalperiksa) {
      dispatch(getDokterPoli(poli, tanggalperiksa));
    } else {
      Alert.alert('Error', 'Tgl, Poli tidak boleh kosong');
    }
  };
  const changeDate = tanggalperiksa => {
    if (tanggalperiksa) {
      setTanggalperiksa(tanggalperiksa);
      dispatch(getPoliVclaim(tanggalperiksa));
    } else {
      Alert.alert('Error', 'Tgl, Poli tidak boleh kosong');
    }
  };

  const getUser = () => {
    getData('userInfo').then(res => {
      if (res) {
        setNama(res.pasien.nama);
        setNorm(res.pasien.no_rekam_medis);
      } else {
        navigation.goBack();
        Alert.alert('Error', 'Silahkan Login Untuk mengakses modul ini', [
          {
            text: 'Login',
            onPress: () => navigation.replace('Login'),
            style: 'login',
          },
          {text: 'Close'},
        ]);
      }
    });
  };

  useEffect(() => {
    getUser();
  }, []);

  useEffect(() => {
    if (ambilAntreanError) {
      Alert.alert('Error', ambilAntreanError);
      dispatchError(dispatch, AMBIL_ANTREAN_ERROR, false);
    }

    if (ambilAntreanData) {
      Alert.alert(
        'Success',
        'Nomor Antrean ' +
          ambilAntreanData.nomorantrean +
          ' ' +
          ambilAntreanData.keterangan,
      );
      dispatchSuccess(dispatch, AMBIL_ANTREAN_SUCCESS, false);
      navigation.replace('MainApp');
    }
  }, [dispatch, ambilAntreanError, ambilAntreanData]);

  return (
    <View style={styles.pages}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Inputan
          label="Nama Peserta"
          value={nama}
          onChangeText={nama => setNama(nama)}
          placeholder="No Rujukan / No Kontrol Pasien"
          disabled
        />

        <Inputan
          label="No Rekam Medis"
          value={norm}
          onChangeText={norm => setNorm(norm)}
          no_rekam_medis="011223"
          disabled
        />

        <Pilihan
          label="Jenis Kunjungan"
          datas={[
            '1 (Datang Sendiri)',
            '2 (Diterima Dari Puskesmas)',
            '3 (Diterima Dari RS. Lain)',
            '4 (Diterima Dari Fasilitas Kes. Lain)',
            '5 (Diterima Kembali)',
          ]}
          selectedValue={jeniskunjungan}
          onValueChange={jeniskunjungan => setJeniskunjungan(jeniskunjungan)}
        />

        <Inputan
          label="NIK"
          value={nik}
          onChangeText={nik => setNik(nik)}
          placeholder="No Induk Kependudukan"
          keyboardType="number-pad"
          maxLength={16}
        />

        <Inputan
          label="No Hp"
          value={nohp}
          onChangeText={nohp => setNohp(nohp)}
          placeholder="Telepon Max 12 Digit"
          keyboardType="number-pad"
          maxLength={12}
        />

        {/* <Inputan
          label="Tgl Kunjungan"
          value={tanggalperiksa}
          onChangeText={tanggalperiksa => setTanggalperiksa(tanggalperiksa)}
        /> */}

        <Inputan
          label="Tgl Rancana Kunjungan"
          placeholder="yyyy-mm-dd"
          value={tanggalperiksa}
          onTouchStart={() => setOpen(!open)}
          showSoftInputOnFocus={false}
        />

        {open && (
          <Calendar
            onDayPress={day => {
              setOpen(!open);
              changeDate(day.dateString);
              setPoli('');
              setDokter('');
            }}
            minDate={DateTime.now().plus({days: 1}).toFormat('yyyy-MM-dd')}
            maxDate={DateTime.now().plus({days: 6}).toFormat('yyyy-MM-dd')}
          />
        )}

        <Pilihan
          label="Poliklinik"
          datas={poliData ? poliData : []}
          selectedValue={poli}
          onValueChange={poli_id => changeDokter(poli_id)}
        />

        <Pilihan
          label="Dokter"
          datas={dokterData ? dokterData : []}
          selectedValue={dokter}
          onValueChange={dokter => setValueDokter(dokter)}
        />

        <View style={styles.submit}>
          <Tombol
            loading={ambilAntreanLoading}
            title="Daftar"
            type="text"
            padding={responsiveHeight(15)}
            fontSize={18}
            onPress={() => onSubmit()}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    paddingHorizontal: 30,
    backgroundColor: colors.white,
    paddingTop: 10,
  },

  inputFoto: {
    marginTop: 20,
  },
  label: {
    fontSize: 18,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  foto: {
    width: responsiveWidth(170),
    height: responsiveHeight(200),
    borderRadius: 40,
  },

  wrapperUpload: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },

  tombolChangePhoto: {
    marginLeft: 20,
    flex: 1,
  },
  submit: {
    marginVertical: 10,
  },
});

export default DaftarPasienNonJkn;
