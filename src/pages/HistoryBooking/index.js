import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  useWindowDimensions,
} from 'react-native';
import {colors, fonts, heightMobileUI, responsiveHeight} from '../../utils';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import {RFValue} from 'react-native-responsive-fontsize';
import {dummyAntrean} from '../../data';
import HistoryBookinList from '../HistoryBookingList';

const BelumRoute = () => {
  return <HistoryBookinList status="Belum" />;
};
const CheckInRoute = () => {
  return <HistoryBookinList status="CheckIn" />;
};
const BatalRoute = () => {
  return <HistoryBookinList status="Batal" />;
};
const GagalRoute = () => {
  return <HistoryBookinList status="Gagal" />;
};

const renderScene = SceneMap({
  belum: BelumRoute,
  checkin: CheckInRoute,
  batal: BatalRoute,
  gagal: GagalRoute,
});

const HistoryBooking = () => {
  const layout = useWindowDimensions();

  const [index, setIndex] = useState(0);
  const [routes] = useState([
    {key: 'belum', title: 'Belum'},
    {key: 'checkin', title: 'CheckIn'},
    {key: 'batal', title: 'Batal'},
    {key: 'gagal', title: 'Gagal'},
  ]);

  return (
    <TabView
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{width: layout.width}}
      renderTabBar={props => (
        <TabBar
          {...props}
          style={{backgroundColor: colors.secondary}}
          indicatorStyle={{backgroundColor: colors.primary}}
          pressColor={colors.primary}
        />
      )}
    />
  );
};

const styles = StyleSheet.create({
  pages: {
    backgroundColor: colors.white,
    flex: 1,
  },

  wrap: {
    marginHorizontal: 30,
    marginVertical: 20,
  },

  cardContainer: {
    backgroundColor: colors.secondary,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    padding: responsiveHeight(10),
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    padding: responsiveHeight(10),
    borderRadius: 10,
    alignItems: 'center',
  },
  card: {
    flex: 1,
    paddingHorizontal: 20,
  },
  infoAntrean: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  antrean: {
    backgroundColor: colors.primary,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  panggail: {
    backgroundColor: colors.primary,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 2,
    borderRadius: 5,
  },
  textAntreanTitle: {
    fontSize: 14,
    fontFamily: fonts.primary.regular,
    color: colors.white,
  },
  textAntrean: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: colors.white,
    marginTop: 5,
  },
  textPoli: {
    fontSize: RFValue(16, heightMobileUI),
    fontFamily: fonts.primary.regular,
    color: colors.white,
  },
  text: {
    fontSize: 16,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  titleHeader: {
    marginBottom: 10,
  },
  textHeader: {
    fontSize: 20,
    fontFamily: fonts.primary.regular,
    alignSelf: 'center',
    color: colors.primary,
  },
});

export default HistoryBooking;
