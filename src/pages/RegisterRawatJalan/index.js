import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import {HealthCare, IconArrowRight, IndonesiaRupiah} from '../../assets';
import {Gap} from '../../components/atoms';
import {ListHistory} from '../../components/molecules';
import {dummyPesanans} from '../../data';
import {colors, fonts, responsiveHeight, responsiveWidth} from '../../utils';

const RegisterRawatJalan = () => {
  const navigation = useNavigation();

  const [pesanans, setPesanans] = useState(dummyPesanans);

  return (
    <View style={styles.pages}>
      <View style={styles.wrap}>
        <TouchableOpacity
          onPress={() => navigation.navigate('NoRujukanPeserta')}
          style={styles.card}>
          <View style={styles.container}>
            <Image source={HealthCare} style={styles.image} />
            <View style={styles.keterangan}>
              <Text style={styles.text}>DAFTAR PESERTA JKN</Text>
              <Gap height={7} />
              <Text>JKN Jaminan Kesehatan Nasional</Text>
            </View>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => navigation.navigate('DaftarPasienNonJkn')}
          style={styles.card}>
          <View style={styles.container}>
            <Image source={IndonesiaRupiah} style={styles.image} />
            <View style={styles.keterangan}>
              <Text style={styles.text}>DAFTAR PESERTA NON JKN</Text>
              <Gap height={7} />
              <Text>Umum Atau Asuransi Lain</Text>
            </View>
          </View>
        </TouchableOpacity>

        <View style={styles.syaratCard}>
          <Text style={styles.syaratTitle}>Syarat Dan Ketentuan</Text>
          <Text style={styles.syarat}>
            1. Pendaftaran online hanya berlaku untuk pasien rawat jalan.
          </Text>
          <Text style={styles.syarat}>
            2. Bagi peserta yang mendaftar dengan BPJS Kesehatan harus memiliki
            rujukan aktif dari Fasilitas Kesehatan Tingkat Pertama (FKTP) atau
            Fasilitas Kesehatan Tingkat Lanjut (FKRTL)
          </Text>
          <Text style={styles.syarat}>
            3. Pendaftaran online hanya bisa di lakukan jika pasien sudah
            terdaftar di SIM Rumah Sakit / Sudah memiliki No Rekam Medis di
            Rumah Sakit.
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    height: 40,
    width: 40,
    borderRadius: 5,
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  keterangan: {
    paddingHorizontal: responsiveWidth(10),
  },
  pages: {
    backgroundColor: colors.white,
    flex: 1,
  },
  wrap: {
    marginHorizontal: 30,
  },
  card: {
    backgroundColor: colors.white,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    // marginHorizontal: 30,
    padding: responsiveHeight(30),
    borderRadius: 10,
    // backgroundColor: colors.white,
  },
  syaratCard: {
    backgroundColor: colors.white,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    padding: responsiveHeight(30),
    borderRadius: 10,
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: colors.drak,
  },
  syaratTitle: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: colors.red,
    marginBottom: 10,
  },
  syarat: {
    fontSize: 14,
    fontFamily: fonts.primary.regular,
    color: colors.red,
    textAlign: 'justify',
    marginVertical: 5,
  },
});

export default RegisterRawatJalan;
