import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  useWindowDimensions,
} from 'react-native';
import {Doctor, Folder, IconArrowRight, MedicalTeam} from '../../assets';
import {Gap} from '../../components/atoms';
import {dummyDokterPoli, dummyPesanans, dummyPoliByJadwal} from '../../data';
import {
  colors,
  fonts,
  responsiveHeight,
  responsiveWidth,
  heightMobileUI,
} from '../../utils';

import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import {RFValue} from 'react-native-responsive-fontsize';
import {useDispatch, useSelector} from 'react-redux';
import {getJadwalDokter, getJadwalPoli} from '../../actions/HomeAction';

const FirstRoute = () => {
  const navigation = useNavigation();

  const dispatch = useDispatch();

  const DokterJadwalRed = useSelector(state => state.DokterJadwalReducer);
  const {dokterJadwalData, dokterJadwalError, dokterJadwalLoading} =
    DokterJadwalRed;

  useEffect(() => {
    dispatch(getJadwalDokter());
  }, []);

  return (
    <View style={styles.pages}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.wrap}>
          {dokterJadwalData.length ? (
            dokterJadwalData.map(data => (
              <TouchableOpacity
                style={styles.container}
                key={data.id}
                onPress={() =>
                  navigation.navigate('ListJadwalDokter', {
                    id: data.id,
                    nama: data.nama,
                    uraian: data.uraian,
                  })
                }>
                <Image source={Doctor} style={styles.image} />
                <View style={styles.informasi}>
                  <Text style={styles.title}>{data.nama}</Text>
                  <Text>{data.uraian}</Text>
                </View>
                <IconArrowRight />
              </TouchableOpacity>
            ))
          ) : dokterJadwalLoading ? (
            <View
              style={{
                marginTop: responsiveHeight(40),
              }}>
              <ActivityIndicator color={colors.primary} />
            </View>
          ) : (
            <View
              style={{
                marginTop: responsiveHeight(40),
                alignItems: 'center',
              }}>
              <Text>List Data Kosong</Text>
            </View>
          )}
        </View>
        <Gap height={20} />
      </ScrollView>
    </View>
  );
};

const SecondRoute = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const PoliJadwalRed = useSelector(state => state.PoliJadwalReducer);
  const {poliJadwalData, poliJadwalError, poliJadwalLoading} = PoliJadwalRed;

  useEffect(() => {
    dispatch(getJadwalPoli());
  }, []);

  return (
    <View style={styles.pages}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.wrap}>
          {poliJadwalData.length ? (
            poliJadwalData.map(data => (
              <TouchableOpacity
                style={styles.container}
                key={data.id}
                onPress={() =>
                  navigation.navigate('ListJadwalDokterPoli', {
                    id: data.id,
                    uraian: data.uraian,
                  })
                }>
                <Image source={Folder} style={styles.image} />
                <View style={styles.informasi}>
                  <Text style={styles.title}>{data.uraian}</Text>
                  <Text>{data.uraian}</Text>
                </View>
                <IconArrowRight />
              </TouchableOpacity>
            ))
          ) : poliJadwalLoading ? (
            <View
              style={{
                marginTop: responsiveHeight(40),
              }}>
              <ActivityIndicator color={colors.primary} />
            </View>
          ) : (
            <View
              style={{
                marginTop: responsiveHeight(40),
                alignItems: 'center',
              }}>
              <Text>List Data Kosong</Text>
            </View>
          )}
        </View>
        <Gap height={20} />
      </ScrollView>
    </View>
  );
};

const renderScene = SceneMap({
  first: FirstRoute,
  second: SecondRoute,
});

const JadwalDokter = () => {
  const layout = useWindowDimensions();

  const [index, setIndex] = useState(0);
  const [routes] = useState([
    {key: 'first', title: 'Dokter'},
    {key: 'second', title: 'Poliklinik'},
  ]);

  return (
    <TabView
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{width: layout.width}}
      renderTabBar={props => (
        <TabBar
          {...props}
          style={{backgroundColor: colors.secondary}}
          indicatorStyle={{backgroundColor: colors.primary}}
          pressColor={colors.primary}
        />
      )}
    />
  );
};

const styles = StyleSheet.create({
  image: {
    height: responsiveWidth(35),
    width: responsiveWidth(35),
    borderRadius: 5,
  },
  informasi: {
    paddingHorizontal: responsiveWidth(20),
    flex: 1,
  },
  pages: {
    backgroundColor: colors.white,
    flex: 1,
  },
  wrap: {
    marginHorizontal: 30,
  },
  container: {
    flexDirection: 'row',
    backgroundColor: colors.white,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    padding: responsiveHeight(10),
    borderRadius: 10,
    alignItems: 'center',
    borderStartWidth: 7,
    borderStartColor: colors.secondary,
  },
  text: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: colors.drak,
  },
  title: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: colors.drak,
    marginBottom: 10,
  },
  display: {
    fontSize: 16,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default JadwalDokter;
