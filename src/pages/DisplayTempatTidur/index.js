import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getDisplayTempatTidur} from '../../actions/HomeAction';
import {Bed2, HealthCare, IconArrowRight, IndonesiaRupiah} from '../../assets';
import {Gap} from '../../components/atoms';
import {ListHistory} from '../../components/molecules';
import {dummyPesanans, dummyTempatTidur} from '../../data';
import {colors, fonts, responsiveHeight, responsiveWidth} from '../../utils';

const DisplayTempatTidur = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const DisplayTempatTidurRed = useSelector(
    state => state.DisplayTempatTidurReducer,
  );
  const {dttData, dttError, dttLoading} = DisplayTempatTidurRed;

  useEffect(() => {
    dispatch(getDisplayTempatTidur());
  }, []);

  return (
    <View style={styles.pages}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.wrap}>
          {dttData.length ? (
            dttData.map(data => (
              <View style={styles.container} key={data.id}>
                <Image source={Bed2} style={styles.image} />
                <View style={styles.card}>
                  <Text style={styles.title}>
                    {data.ruangan} {data.ruang}
                  </Text>
                  <View style={styles.display}>
                    <Text style={styles.text}>Kapasitas</Text>
                    <Text style={styles.text}>{data.jumlah_kamar}</Text>
                  </View>
                  <View style={styles.display}>
                    <Text style={styles.text}>Tersedia</Text>
                    <Text style={styles.text}>{data.kosong}</Text>
                  </View>
                  <View style={styles.display}>
                    <Text style={styles.text}>Terpakai</Text>
                    <Text style={styles.text}>{data.terpakai}</Text>
                  </View>
                  <View style={styles.updated}>
                    <Text style={styles.updatedAt}>{data.updated_at}</Text>
                  </View>
                </View>
              </View>
            ))
          ) : dttLoading ? (
            <View
              style={{
                marginTop: responsiveHeight(40),
              }}>
              <ActivityIndicator color={colors.primary} />
            </View>
          ) : (
            <View
              style={{
                marginTop: responsiveHeight(40),
                alignItems: 'center',
              }}>
              <Text>List Data Kosong</Text>
            </View>
          )}
        </View>
        <Gap height={20} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    height: responsiveWidth(50),
    width: responsiveWidth(50),
    borderRadius: 5,
  },
  card: {
    paddingHorizontal: responsiveWidth(20),
    flex: 1,
  },
  pages: {
    backgroundColor: colors.white,
    flex: 1,
  },
  wrap: {
    flex: 1,
    marginHorizontal: 30,
  },
  container: {
    flexDirection: 'row',
    backgroundColor: colors.white,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    padding: responsiveHeight(10),
    borderRadius: 10,
    alignItems: 'center',
    borderEndWidth: 7,
    borderRightColor: colors.secondary,
  },
  text: {
    fontSize: 15,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  title: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: colors.drak,
    marginBottom: 10,
  },
  updatedAt: {
    fontSize: 14,
    fontFamily: fonts.primary.regular,
    color: colors.red,
    marginBottom: 10,
    fontStyle: 'italic',
  },
  display: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  updated: {
    marginTop: 10,
  },
  loading: {
    flex: 1,
    marginTop: 10,
    marginBottom: 30,
  },
});

export default DisplayTempatTidur;
