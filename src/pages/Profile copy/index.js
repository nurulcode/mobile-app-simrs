import React, {useState} from 'react';
import {useEffect} from 'react';
import {View, Text, StyleSheet, Image, Alert} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {DefaultImage, FotoProfile} from '../../assets';
import {ListMenu} from '../../components/molecules';
import {dummyProfile, dummyMenu} from '../../data';
import {
  colors,
  fonts,
  getData,
  responsiveHeight,
  responsiveWidth,
} from '../../utils';
import {heightMobileUI} from '../../utils';

const Profile = ({navigation}) => {
  const [profile, setProfile] = useState(false);
  const [menus, setMenus] = useState(dummyMenu);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getUserData();

      // if (!profile) {
      //   Alert.alert('Login', 'Maaf, Anda tidak bisa mengakses menu ini');
      //   navigation.goBack();
      // } else {
      //   getUserData();
      // }
    });

    return unsubscribe;
  }, [navigation]);

  function getUserData() {
    getData('user').then(res => {
      const data = res;

      if (data) {
        setProfile(data);
      } else {
        navigation.replace('Login');
      }
    });
  }

  return (
    <View style={styles.page}>
      <View style={styles.container}>
        <Image
          source={profile.avatar ? {uri: profile.avatar} : DefaultImage}
          style={styles.foto}
        />
        <View style={styles.profile}>
          <Text style={styles.nama}>{profile.nama}</Text>
          <Text style={styles.desc}>No. HP : {profile.nohp}</Text>
          <Text style={styles.desc}>{profile.alamat}</Text>
        </View>

        <ListMenu menus={menus} navigation={navigation} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  container: {
    position: 'absolute',
    bottom: 0,
    height: responsiveHeight(680),
    width: '100%',
    backgroundColor: colors.white,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
  },
  foto: {
    width: responsiveWidth(150),
    height: responsiveHeight(150),
    borderRadius: 40,
    alignSelf: 'center',
    marginTop: -responsiveWidth(75),
  },
  profile: {
    marginTop: 10,
    alignItems: 'center',
  },
  nama: {
    fontFamily: fonts.primary.bold,
    fontSize: RFValue(24, heightMobileUI),
    color: colors.drak,
  },
  desc: {
    fontFamily: fonts.primary.regular,
    fontSize: RFValue(18, heightMobileUI),
    color: colors.drak,
  },
});

export default Profile;
