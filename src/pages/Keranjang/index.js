import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {useEffect} from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getListKeranjang} from '../../actions/KeranjangAction';
import {Tombol} from '../../components/atoms';
import {ListKeranjang} from '../../components/molecules';
import {dummyPesanans} from '../../data';
import {
  colors,
  fonts,
  getData,
  numberWithCommas,
  responsiveWidth,
} from '../../utils';

const Keranjang = props => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const data = useSelector(state => state.ListKeranjangReducer);
  const {listKeranjangData, listKeranjangLoading, listKeranjangError} = data;

  const remove = useSelector(state => state.removeKeranjangReducer);
  const {removeKeranjangData, removeKeranjangLoading, removeKeranjangError} =
    remove;

  useEffect(() => {
    getData('user').then(res => {
      if (res) {
        dispatch(getListKeranjang(res.uid));
      } else {
        navigation.goBack();
        Alert.alert('Error', 'Silahkan Login Untuk mengakses modul ini', [
          {
            text: 'Login',
            onPress: () => navigation.replace('Login'),
            style: 'login',
          },
          {text: 'Close'},
        ]);
        // props.navigation.replace("Login")
      }
    });
  }, [dispatch, removeKeranjangData]);

  return (
    <View style={styles.page}>
      <ListKeranjang {...data} />

      {listKeranjangData && (
        <View style={styles.footer}>
          <View style={styles.totalHarga}>
            <Text style={styles.text}>Total Harga : </Text>
            <Text style={styles.text}>
              Rp.{' '}
              {listKeranjangData.totalHarga
                ? numberWithCommas(listKeranjangData.totalHarga)
                : 0}
            </Text>
          </View>

          {listKeranjangData ? (
            <Tombol
              title="Check Out"
              type="textIcon"
              fontSize={18}
              padding={responsiveWidth(15)}
              icon="keranjang-putih"
              onPress={() =>
                props.navigation.navigate('Checkout', {
                  totalHarga: listKeranjangData.totalHarga,
                  totalBerat: listKeranjangData.totalBerat,
                })
              }
            />
          ) : (
            <Tombol
              title="Check Out"
              type="textIcon"
              fontSize={18}
              padding={responsiveWidth(15)}
              icon="keranjang-putih"
              onPress={() => props.navigation.navigate('Checkout')}
              disabled={true}
            />
          )}
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.white,
  },
  footer: {
    paddingHorizontal: 30,
    backgroundColor: colors.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    paddingBottom: 30,
  },
  totalHarga: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 20,
  },
  text: {
    fontSize: 18,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
});

export default Keranjang;
