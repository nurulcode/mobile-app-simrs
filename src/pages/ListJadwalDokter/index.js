import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {useDispatch, useSelector} from 'react-redux';
import {getJadwalDokterDetail} from '../../actions/HomeAction';
import {Gap} from '../../components/atoms';
import {
  colors,
  fonts,
  heightMobileUI,
  responsiveHeight,
  responsiveWidth,
} from '../../utils';

const ListJadwalDokter = props => {
  const params = props.route.params;
  const dispatch = useDispatch();

  const DokterJadwalDetailRed = useSelector(
    state => state.DokterJadwalDetailReducer,
  );
  const {
    dokterJadwalDetailData,
    dokterJadwalDetailError,
    dokterJadwalDetailLoading,
  } = DokterJadwalDetailRed;

  useEffect(() => {
    if (params.id) {
      dispatch(getJadwalDokterDetail(params.id));
    }
  }, []);

  return (
    <View style={styles.pages}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.wrap}>
          <View style={styles.container}>
            <View style={styles.card}>
              <Text style={styles.textTitle}>{props.route.params.nama}</Text>
              <Text style={styles.textTitle}>{props.route.params.uraian}</Text>
              <Gap height={20} />
              {dokterJadwalDetailData.list ? (
                dokterJadwalDetailData.list.map(data => (
                  <View key={data.id}>
                    <View style={styles.display}>
                      <Text style={styles.text}>Hari</Text>
                      <Text style={styles.text}>{data.hari}</Text>
                    </View>
                    <View style={styles.display}>
                      <Text style={styles.text}>Pukul</Text>
                      <Text style={styles.text}>
                        {data.mulai} - {data.selesai}
                      </Text>
                    </View>
                    <View style={styles.display}>
                      <Text style={styles.text}>Kuota</Text>
                      <Text style={styles.text}>{data.kuota}</Text>
                    </View>

                    <View
                      style={{
                        borderBottomColor: 'black',
                        borderBottomWidth: StyleSheet.hairlineWidth,
                        marginVertical: 10,
                      }}
                    />
                  </View>
                ))
              ) : dokterJadwalDetailLoading ? (
                <View
                  style={{
                    marginTop: responsiveHeight(40),
                  }}>
                  <ActivityIndicator color={colors.primary} />
                </View>
              ) : (
                <View
                  style={{
                    marginTop: responsiveHeight(40),
                    alignItems: 'center',
                  }}>
                  <Text>List Data Kosong</Text>
                </View>
              )}
            </View>
          </View>
        </View>
        <Gap height={20} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    paddingHorizontal: responsiveWidth(20),
    flex: 1,
  },
  pages: {
    backgroundColor: colors.white,
    flex: 1,
  },
  wrap: {
    marginHorizontal: 30,
  },
  container: {
    flexDirection: 'row',
    backgroundColor: colors.white,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    padding: responsiveHeight(10),
    borderRadius: 10,
    alignItems: 'center',
    borderEndWidth: 7,
    borderRightColor: colors.secondary,
  },
  text: {
    fontSize: 16,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  textTitle: {
    fontSize: RFValue(18, heightMobileUI),
    fontFamily: fonts.primary.regular,
    color: colors.primary,
    textAlign: 'center',
  },
  display: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default ListJadwalDokter;
