import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Alert} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {loginUser} from '../../actions/AuthAction';
import {Ilustrasi, LogoNew} from '../../assets';
import Logo from '../../assets/images/logo.svg';
import {Gap, Inputan, Pilihan, Tombol} from '../../components/atoms';
import {colors, fonts, getData, responsiveHeight} from '../../utils';
const Login = props => {
  const [norm, setNoRm] = useState('000015');
  const [tgl, setTgl] = useState('2000-02-14');

  const dispatch = useDispatch();
  const data = useSelector(state => state.LoginReducer);
  const {loginLoading, loginData, loginError} = data;

  useEffect(() => {
    if (loginError) {
      Alert.alert('Error', loginError);
    }

    getData('userInfo').then(res => {
      if (res) {
        props.navigation.replace('MainApp');
      }
    });
  }, [dispatch, loginData]);

  const login = () => {
    if (norm && tgl) {
      dispatch(loginUser(norm, tgl));
    } else {
      Alert.alert('Error', 'No RM Dan Tgl tidak boleh kosong!');
    }
  };

  return (
    <View style={styles.pages}>
      {/* <View style={styles.logo}>
        <LogoNew />
      </View> */}

      <View style={styles.cardLogin}>
        <Inputan
          label="No Rekam Medis"
          value={norm}
          onChangeText={norm => setNoRm(norm)}
          placeholder="000001"
        />
        <Inputan
          label="Tanggal Lahir"
          value={tgl}
          onChangeText={tgl => setTgl(tgl)}
          placeholder="yyyy-mm-dd"
        />
        <Gap height={25} />
        <Tombol
          type="text"
          title="Login"
          fontSize={18}
          padding={12}
          loading={loginLoading}
          onPress={() => login()}
        />
      </View>

      <View>
        <TouchableOpacity
          style={styles.register}
          onPress={() => props.navigation.replace('MainApp')}>
          <Text style={styles.textBlue}>Beranda</Text>
        </TouchableOpacity>
      </View>

      {/* <View style={styles.ilustrasi}>
        <Ilustrasi />
      </View> */}
    </View>
  );
};

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    backgroundColor: colors.white,
    justifyContent: 'center',
  },
  ilustrasi: {
    position: 'absolute',
    bottom: 0,
    right: -100,
  },
  logo: {
    alignItems: 'center',
    marginTop: responsiveHeight(150),
    marginBottom: responsiveHeight(40),
  },

  cardLogin: {
    backgroundColor: colors.white,
    marginHorizontal: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    padding: 30,
    // borderRadius: 10,
    marginTop: 10,
  },
  register: {
    alignItems: 'center',
    marginTop: 10,
  },
  textBlue: {
    color: colors.primary,
    fontFamily: fonts.primary.regular,
    fontSize: 18,
  },
});

export default Login;
