import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {useEffect} from 'react';
import {View, StyleSheet, Alert} from 'react-native';
import {Calendar} from 'react-native-calendars';
import {useDispatch, useSelector} from 'react-redux';
import {
  cariRencanaKontrolVclaim,
  cariRujukanVclaim,
} from '../../actions/RujukanAction';
import {Gap, Inputan, Pilihan, Tombol} from '../../components/atoms';
import {
  AMBIL_ANTREAN_RESET,
  AMBIL_ANTREAN_SUCCESS,
} from '../../constants/AntreanConstat';
import {
  colors,
  dispatchSuccess,
  fonts,
  getData,
  responsiveHeight,
} from '../../utils';

const NoRujukanPeserta = () => {
  const navigation = useNavigation();
  const [open, setOpen] = useState(false);
  const [noSurat, setNoRujukan] = useState('');
  const [jnsKunjungan, setJnsKunjungan] = useState('');
  const [user, setUser] = useState(false);

  const dispatch = useDispatch();

  const rujukan = useSelector(state => state.RujukanReducer);
  const {rujukanData, rujukanError, rujukanLoading} = rujukan;

  const cariRujukan = () => {
    if (!noSurat) {
      Alert.alert('Error', 'Nomor Rujukan Tidak Boleh Kosong');
    } else if (!jnsKunjungan) {
      Alert.alert('Error', 'Jenis Kunjungan Tidak Boleh Kosong');
    } else {
      if (jnsKunjungan == '3 (Kontrol)') {
        dispatch(cariRencanaKontrolVclaim(noSurat));
      } else {
        dispatch(cariRujukanVclaim(noSurat));
      }
    }
  };

  useEffect(() => {
    if (noSurat && rujukanData) {
      if (jnsKunjungan != '3 (Kontrol)') {
        navigation.navigate('DaftarPasienJkn', {
          noRef: rujukanData.rujukan.noKunjungan,
          tgl: '',
          jnsKunjungan: jnsKunjungan,
          user: user,
        });
      } else {
        navigation.navigate('DaftarPasienJkn', {
          noRef: rujukanData.noSuratKontrol,
          tgl: rujukanData.tglRencanaKontrol,
          jnsKunjungan: jnsKunjungan,
          user: user,
        });
      }
    }
  }, [dispatch, rujukanData, rujukanLoading]);

  useEffect(() => {
    getData('userInfo').then(res => {
      if (res) {
        setUser(res);
      } else {
        navigation.goBack();
        Alert.alert('Error', 'Silahkan Login Untuk mengakses modul ini', [
          {
            text: 'Login',
            onPress: () => navigation.replace('Login'),
            style: 'login',
          },
          {text: 'Home', onPress: () => navigation.replace('MainApp')},
        ]);
      }
    });
  }, []);

  return (
    <View style={styles.pages}>
      <View>
        <Inputan
          label="No Rujukan Yang Masih Berlaku"
          placeholder="260301010622P000001"
          value={noSurat}
          onChangeText={noSurat => setNoRujukan(noSurat)}
        />

        <Pilihan
          label="Jenis Kunjungan"
          datas={[
            '1 (Rujukan FKTP)',
            '2 (Rujukan Internal)',
            '3 (Kontrol)',
            '4 (Rujukan Antar RS)',
          ]}
          selectedValue={jnsKunjungan}
          onValueChange={jnsKunjungan => setJnsKunjungan(jnsKunjungan)}
        />

        {/* <Inputan
            label="Tgl Rencana Kunjungan"
            placeholder="yyyy-mm-dd"
            value={date}
            onTouchStart={() => setOpen(!open)}
            showSoftInputOnFocus={false}
          /> */}

        <Gap height={7} />
        {/* <Tombol
          title="Tanggal"
          type="text"
          padding={responsiveHeight(15)}
          fontSize={18}
          onPress={() => setOpen(!open)}
        /> */}

        {open && (
          <Calendar
            onDayPress={day => {
              setDate(day.dateString);
              setOpen(!open);
            }}
          />
        )}
      </View>

      <View style={styles.submit}>
        <Tombol
          loading={rujukanLoading}
          title="Cari No Rujukan"
          type="text"
          padding={responsiveHeight(15)}
          fontSize={18}
          onPress={() => cariRujukan()}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    paddingHorizontal: 30,
    backgroundColor: colors.white,
    paddingTop: 10,
    justifyContent: 'space-between',
  },
  label: {
    fontSize: 18,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  submit: {
    marginVertical: 10,
  },
});

export default NoRujukanPeserta;
