import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import {HealthCare, IconArrowRight, IndonesiaRupiah} from '../../assets';
import {Gap} from '../../components/atoms';
import {ListHistory} from '../../components/molecules';
import {dummyPesanans} from '../../data';
import {colors, fonts, responsiveHeight, responsiveWidth} from '../../utils';

const KontakKami = () => {
  const navigation = useNavigation();

  const [pesanans, setPesanans] = useState(dummyPesanans);

  return (
    <View style={styles.pages}>
      <ScrollView>
        <View style={styles.wrap}>
          <Gap height={10} />

          <View style={styles.card}>
            <Text style={styles.title}>INFORMASI</Text>

            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: StyleSheet.hairlineWidth,
                marginVertical: 15,
              }}
            />
            <View style={styles.container}>
              <Text style={styles.title}>Telepon</Text>
              <Text>(0986) 215133</Text>
            </View>
            <View style={styles.container}>
              <Text style={styles.title}>E-Mail</Text>
              <Text>rsu.manokwari@gmail.com</Text>
            </View>
            <View style={styles.container}>
              <Text style={styles.title}>Website</Text>
              <Text>https://rsud.manokwarikab.go.id/</Text>
            </View>
            <View style={styles.container}>
              <Text style={styles.title}>Alamat</Text>
              <Text>
                Jl. Siliwangi No.1, Manokwari Timmur. Kec. Manokwari Barat
              </Text>
            </View>
          </View>
        </View>
        <Gap height={30} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    height: 40,
    width: 40,
    borderRadius: 5,
  },
  container: {
    justifyContent: 'space-between',
  },
  keterangan: {
    paddingHorizontal: responsiveWidth(10),
  },
  pages: {
    backgroundColor: colors.white,
    flex: 1,
  },
  wrap: {
    marginHorizontal: 30,
  },
  card: {
    backgroundColor: colors.white,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    // marginHorizontal: 30,
    padding: responsiveHeight(30),
    borderEndWidth: 7,
    borderEndColor: colors.primary,
  },
  text: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: colors.drak,
  },
  title: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: colors.primary,
    marginTop: 10,
  },
});

export default KontakKami;
