import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, Image, Alert} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {
  colors,
  fonts,
  responsiveHeight,
  responsiveWidth,
  numberWithCommas,
  getData,
} from '../../utils';
import {heightMobileUI} from '../../utils';
import {CardLiga, Gap, Inputan, Pilihan, Tombol} from '../../components/atoms';
import {JerseySlider} from '../../components/molecules';
import {useDispatch, useSelector} from 'react-redux';
import {getLigaById} from '../../actions/LigaAction';
import {masukKeranjang} from '../../actions/KeranjangAction';
import {useNavigation} from '@react-navigation/native';

const JerseyDetail = props => {
  const navigation = useNavigation();

  const [jersey, setJersey] = useState(props.route.params.jersey);
  const [images, setImages] = useState(props.route.params.jersey.gambar);
  const [jumlah, setJumlah] = useState('');
  const [keterangan, setKeterangan] = useState('');
  const [ukuran, setUkuran] = useState('');
  const [uid, setUid] = useState('');
  const dispatch = useDispatch();

  const data = useSelector(state => state.LigaByIdReducer);
  const {ligaDetailData, ligaDetailError, ligaDetailLoading} = data;

  const cart = useSelector(state => state.KeranjangReducer);
  const {keranjangData, keranjangError, keranjangLoading, keranjangStatus} =
    cart;
  useEffect(() => {
    dispatch(getLigaById(jersey.liga));
  }, []);

  useEffect(() => {
    if (keranjangStatus) {
      props.navigation.replace('Keranjang');
    }
  }, [keranjangStatus]);

  const onCart = () => {
    getData('user').then(res => {
      if (res) {
        const id = res.uid;

        if (jumlah && ukuran && keterangan && id) {
          let data = {jersey, images, jumlah, keterangan, ukuran, id};
          dispatch(masukKeranjang(data));
        } else {
          Alert.alert(
            'Error',
            'Jumlah, Ukuran dan Keterangan tidak boleh kosong',
          );
        }
      } else {
        navigation.goBack();
        Alert.alert('Error', 'Silahkan Login Untuk mengakses modul ini', [
          {
            text: 'Login',
            onPress: () => navigation.replace('Login'),
            style: 'login',
          },
          {text: 'Close'},
        ]);
      }
    });
  };

  return (
    <View style={styles.page}>
      <View style={styles.button}>
        <Tombol
          padding={7}
          icon="arrow-left"
          onPress={() => props.navigation.goBack()}
        />
      </View>

      <JerseySlider images={jersey.gambar} />

      <View style={styles.container}>
        <View style={styles.liga}>
          <CardLiga liga={ligaDetailData} />
        </View>
        <View style={styles.desc}>
          <Text style={styles.nama}>{jersey.nama}</Text>
          <Text style={styles.harga}>
            Harga : Rp. {numberWithCommas(jersey.harga)}
          </Text>

          <View style={styles.garis} />

          <View style={styles.wrapperJenisBerat}>
            <Text style={styles.jenisBerat}>Jenis : {jersey.jenis}</Text>
            <Text style={styles.jenisBerat}>Berat : {jersey.berat}</Text>
          </View>

          <View style={styles.wrapperInput}>
            <Inputan
              label="Jumlah"
              width={responsiveWidth(166)}
              height={responsiveHeight(43)}
              fontSize={13}
              value={jumlah}
              onChangeText={jumlah => setJumlah(jumlah)}
            />
            <Pilihan
              label="Pilih Ukuran"
              width={responsiveWidth(166)}
              height={responsiveHeight(43)}
              fontSize={13}
              datas={jersey.ukuran}
              selectedValue={ukuran}
              onValueChange={ukuran => setUkuran(ukuran)}
            />
          </View>
          <Inputan
            textarea
            label="Keterangan"
            fontSize={13}
            placeholder="Isi jika ingin mebambahkan name tag (nama & angka)"
            value={keterangan}
            onChangeText={keterangan => setKeterangan(keterangan)}
          />

          <Gap height={15} />

          <Tombol
            title="Masukan Keranjang"
            type="textIcon"
            icon="keranjang-putih"
            padding={responsiveHeight(17)}
            fontSize={13}
            onPress={() => onCart()}
            loading={keranjangLoading}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  container: {
    position: 'absolute',
    bottom: 0,
    height: responsiveHeight(465),
    width: '100%',
    backgroundColor: colors.white,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
  },
  button: {
    position: 'absolute',
    top: 30,
    left: 30,
    zIndex: 1,
  },
  nama: {
    fontFamily: fonts.primary.regular,
    fontSize: RFValue(24, heightMobileUI),
    textTransform: 'capitalize',
    color: colors.drak,
  },
  harga: {
    fontFamily: fonts.primary.light,
    fontSize: RFValue(24, heightMobileUI),
    color: colors.drak,
  },
  desc: {
    marginHorizontal: 30,
  },
  liga: {
    alignItems: 'flex-end',
    marginRight: 30,
    marginTop: -30,
  },
  garis: {
    borderWidth: 0.5,
    marginVertical: 5,
  },
  wrapperJenisBerat: {
    flexDirection: 'row',
    marginBottom: 5,
  },
  jenisBerat: {
    fontSize: 13,
    fontFamily: fonts.primary.regular,
    marginRight: 30,
    color: colors.drak,
  },
  wrapperInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default JerseyDetail;
