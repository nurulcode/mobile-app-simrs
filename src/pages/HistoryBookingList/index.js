import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {listBookingAntrean} from '../../actions/KunjunganAction';
import {Bed2, HealthCare, IconArrowRight, IndonesiaRupiah} from '../../assets';
import {Gap, Tombol} from '../../components/atoms';
import {ListHistory} from '../../components/molecules';
import {dummyBooking} from '../../data';
import {
  colors,
  fonts,
  getData,
  responsiveHeight,
  responsiveWidth,
} from '../../utils';

const HistoryBookingList = props => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [date, setDate] = useState(new Date());

  const BookingRed = useSelector(state => state.BookingReducer);
  const {bookingData, bookingLoading, bookingError} = BookingRed;

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getData('userInfo').then(res => {
        if (res) {
          dispatch(listBookingAntrean(res.pasien.no_rekam_medis));
        }
      });
    });
  }, [navigation]);

  const onSubmit = status => {
    if (status == 'CheckIn') {
      Alert.alert('CheckIn', 'Apa anda ingin melakukan checkin ?', [
        {
          text: 'Tidak',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Ya', onPress: () => console.log('OK Pressed')},
      ]);
    } else {
      Alert.alert('Batal');
    }
  };

  return (
    <View style={styles.pages}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.wrap}>
          {bookingData ? (
            bookingData.map((data, index) => (
              <View key={index}>
                {data.status == props.status && (
                  <View style={styles.container}>
                    <View style={styles.card}>
                      <View style={styles.display}>
                        <Text style={styles.text}>Booking</Text>
                        <Text style={styles.text}>{data.no_booking}</Text>
                      </View>
                      <View style={styles.display}>
                        <Text style={styles.text}>Unit</Text>
                        <Text style={styles.text}>{data.uraian_poli}</Text>
                      </View>
                      <View style={styles.display}>
                        <Text style={styles.text}>Tgl Periksa</Text>
                        <Text style={styles.text}>{data.tgl_periksa}</Text>
                      </View>
                      <View style={styles.display}>
                        <Text style={styles.text}>No Antrean</Text>
                        <Text style={styles.text}>{data.no_antrean}</Text>
                      </View>
                      <View style={styles.display}>
                        <Text style={styles.text}>Status</Text>
                        <Text style={styles.text}>{data.status}</Text>
                      </View>

                      {data.validasi ? (
                        <View style={styles.updated}>
                          <Text style={styles.updatedAt}>{data.validasi}</Text>
                        </View>
                      ) : (
                        <View style={styles.updated}>
                          <Text style={styles.updatedAt}>
                            Lakukan CheckIn pada tgl {data.tgl_periksa} , pada
                            jam praktek dokter. Klik menu pemberitahuan untuk
                            mendapatkan info terbaru terkait jadwal dokter,
                            ketersediaan dokter sebelum ke rumah sakit
                          </Text>
                        </View>
                      )}

                      {data.tgl_periksa > date && (
                        <View>
                          {data.status == 'Belum' && (
                            <View style={styles.display}>
                              <Tombol
                                title={date}
                                type="text"
                                icon="submit"
                                color={colors.red}
                                padding={responsiveHeight(10)}
                                fontSize={16}
                                onPress={() =>
                                  navigation.navigate('PembatalanAntrean', {
                                    no_booking: data.no_booking,
                                  })
                                }
                              />

                              <Tombol
                                title="CHECKIN"
                                type="text"
                                icon="submit"
                                padding={responsiveHeight(10)}
                                fontSize={16}
                                onPress={() => onSubmit('CheckIn')}
                              />
                            </View>
                          )}
                        </View>
                      )}
                    </View>
                  </View>
                )}
              </View>
            ))
          ) : bookingLoading ? (
            <View
              style={{
                marginTop: responsiveHeight(40),
              }}>
              <ActivityIndicator color={colors.primary} />
            </View>
          ) : (
            <View
              style={{
                marginTop: responsiveHeight(40),
                alignItems: 'center',
              }}>
              <Text>List Data Kosong</Text>
            </View>
          )}
        </View>
        <Gap height={20} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    height: responsiveWidth(50),
    width: responsiveWidth(50),
    borderRadius: 5,
  },
  card: {
    paddingHorizontal: responsiveWidth(20),
    flex: 1,
  },
  pages: {
    backgroundColor: colors.white,
    flex: 1,
  },
  wrap: {
    marginHorizontal: 30,
  },
  container: {
    flexDirection: 'row',
    backgroundColor: colors.white,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    padding: responsiveHeight(10),
    borderRadius: 10,
    alignItems: 'center',
    borderEndWidth: 7,
    borderRightColor: colors.secondary,
  },
  text: {
    fontSize: 15,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  title: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: colors.drak,
  },
  updatedAt: {
    fontSize: 14,
    fontFamily: fonts.primary.regular,
    color: colors.red,
    marginBottom: 10,
    fontStyle: 'italic',
  },
  display: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  updated: {
    marginTop: 10,
  },
});

export default HistoryBookingList;
