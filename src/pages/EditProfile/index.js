import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, ScrollView, Image, Alert} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getKotaList, getProvinsiList} from '../../actions/RajaOngkirAction';
import {DefaultImage} from '../../assets';
import {Inputan, Pilihan, Tombol} from '../../components/atoms';
import {launchImageLibrary} from 'react-native-image-picker';
import {
  colors,
  fonts,
  getData,
  responsiveHeight,
  responsiveWidth,
} from '../../utils';
import {updateDataProfile} from '../../actions/ProfileAction';

const EditProfile = ({navigation}) => {
  const [uid, setUid] = useState('');
  const [nama, setNama] = useState('');
  const [email, setEmail] = useState('');
  const [nohp, setNohp] = useState('');
  const [alamat, setAlamat] = useState('');
  const [provinsi, setProvinsi] = useState(false);
  const [kota, setKota] = useState(false);

  const [avatar, setAvatar] = useState(false);
  const [avatarForDB, setAvatarForDB] = useState('');
  const [avatarLama, setAvatarLama] = useState('');
  const [updateAvatar, setUpdateAvatar] = useState(false);

  const dispatch = useDispatch();
  const data = useSelector(state => state.RajaOngkirReducer);
  const dataProfile = useSelector(state => state.ProfileReducer);
  const {provinsiData, kotaData} = data;
  const {updateProfileData, updateProfileLoading, updateProfileError} =
    dataProfile;

  const changeProvinsi = provinsi_id => {
    setProvinsi(provinsi_id);
    dispatch(getKotaList(provinsi_id));
  };

  useEffect(() => {
    getUserData();
    dispatch(getProvinsiList());

    if (updateProfileData) {
      Alert.alert("Success", "Data berhasil di ubah.")
      navigation.replace('MainApp');
    }
  }, [dispatch, updateProfileData]);

  const getUserData = () => {
    getData('user').then(res => {
      setUid(res.uid);
      setNama(res.nama);
      setEmail(res.email);
      setNohp(res.nohp);
      setAlamat(res.alamat);
      setProvinsi(res.provinsi);
      setKota(res.kota);
      setAvatar(res.avatar);
      setAvatarLama(res.avatar);

      dispatch(getKotaList(res.provinsi));
    });
  };

  const getImage = () => {
    launchImageLibrary(
      {
        quality: 1,
        maxWidth: 500,
        maxHeight: 500,
        includeBase64: true,
        selectionLimit: 1,
        cameraType: 'front',
        mediaType: 'photo',
      },
      res => {
        if (res.errorCode || res.errorMessage) {
          Alert.alert('Error', 'Terjadi kesalahan saat update foto');
        } else {
          const source = res.assets[0].uri;
          const asset = res.assets[0];
          const fileString = `data:${asset.type};base64,${asset.base64}`;

          setAvatar(source);
          setAvatarForDB(fileString);
          setUpdateAvatar(true);
        }
      },
    );
  };

  const onSubmit = () => {
    if (nama && nohp && alamat && provinsi && kota && uid && email) {
      const data = {
        nama,
        nohp,
        alamat,
        provinsi,
        kota,
        uid,
        avatar,
        email,
        avatarForDB,
        avatarLama,
        updateAvatar
      };

      dispatch(updateDataProfile(data));
    } else {
      Alert.alert('Error', 'Data tidak boleh kosong');
    }
  };

  return (
    <View style={styles.pages}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Inputan
          label="Nama"
          value={nama}
          onChangeText={nama => setNama(nama)}
        />

        <Inputan
          label="Email"
          value={email}
          onChangeText={email => setEmail(email)}
          disabled
        />
        <Inputan
          label="No. Handphone"
          value={nohp}
          onChangeText={nohp => setNohp(nohp)}
          keyboardType="number-pad"
        />
        <Inputan
          label="Alamat"
          value={alamat}
          textarea
          onChangeText={alamat => setAlamat(alamat)}
        />

        <Pilihan
          label="Provinsi"
          datas={provinsiData ? provinsiData : []}
          selectedValue={provinsi}
          onValueChange={provinsi_id => changeProvinsi(provinsi_id)}
        />

        <Pilihan
          label="Kab/Kota"
          datas={kotaData ? kotaData : []}
          selectedValue={kota}
          onValueChange={kota => setKota(kota)}
        />

        <View style={styles.inputFoto}>
          <Text style={styles.label}>Foto Profile : </Text>

          <View style={styles.wrapperUpload}>
            <Image
              source={avatar ? {uri: avatar} : DefaultImage}
              style={styles.foto}
            />

            <View style={styles.tombolChangePhoto}>
              <Tombol
                title="Ubah Foto"
                type="text"
                padding={7}
                onPress={() => getImage()}
              />
            </View>
          </View>
        </View>

        <View style={styles.submit}>
          <Tombol
            loading={updateProfileLoading}
            title="Simpan"
            type="textIcon"
            icon="submit"
            padding={responsiveHeight(15)}
            fontSize={18}
            onPress={() => onSubmit()}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    paddingHorizontal: 30,
    backgroundColor: colors.white,
    paddingTop: 10,
  },

  inputFoto: {
    marginTop: 20,
  },
  label: {
    fontSize: 18,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  foto: {
    width: responsiveWidth(170),
    height: responsiveHeight(200),
    borderRadius: 40,
  },

  wrapperUpload: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },

  tombolChangePhoto: {
    marginLeft: 20,
    flex: 1,
  },
  submit: {
    marginVertical: 10,
  },
});

export default EditProfile;
