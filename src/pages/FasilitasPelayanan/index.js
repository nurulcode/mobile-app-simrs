import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import {HealthCare, IconArrowRight, IndonesiaRupiah} from '../../assets';
import {Gap} from '../../components/atoms';
import {ListHistory} from '../../components/molecules';
import {dummyPesanans} from '../../data';
import {colors, fonts, responsiveHeight, responsiveWidth} from '../../utils';

const FasilitasPelayanan = () => {
  const navigation = useNavigation();

  const [pesanans, setPesanans] = useState(dummyPesanans);

  return (
    <View style={styles.pages}>
      <ScrollView>
        <View style={styles.wrap}>
          <Gap height={10} />
          <View style={styles.header}>
            <Text style={styles.headerText}>
              Rumah Sakit kami menyediakan layanan-layanan seperti, IGD,Rawat
              Jalan, Rawat Inap dan Fasilitas Penunajng lainya.
            </Text>
          </View>

          <View style={styles.syaratCard}>
            <Text style={styles.syaratTitle}>Instalasi Gawat Darurat</Text>
            <Text style={styles.syarat}>
             - layanan yang disediakan untuk kebutuhan pasien yang dalam kondisi gawat darurat dan harus segera dibawa ke rumah sakit.
            </Text>
          </View>

          <View style={styles.syaratCard}>
            <Text style={styles.syaratTitle}>Instalasi Rawat Jalan</Text>
            <Text style={styles.syarat}>
             - Instalasi di rumah sakit yang memberikan pelayanan rawat jalan kepada pasien, sesuai dengan spesialisasi.
            </Text>
          </View>

          <View style={styles.syaratCard}>
            <Text style={styles.syaratTitle}>Instalasi Rawat Inap</Text>
            <Text style={styles.syarat}>
              - Pengobatan atau rehabilitasi oleh tenaga pelayanan kesehatan profesional pada pasien yang sakit, dengan cara di inapkan di ruangan.
            </Text>
          </View>

          <View style={styles.syaratCard}>
            <Text style={styles.syaratTitle}>
              Fasilitas Dan Layanan Penunjang
            </Text>
            <Text style={styles.syarat}>
              - Daftar Layanan Penunjang:
            </Text>
            <Text style={styles.syarat}>
              1. Radiologi
            </Text>
            <Text style={styles.syarat}>
              2. Radioterapi
            </Text>
            <Text style={styles.syarat}>
              3. Patologi Anatomi
            </Text>
            <Text style={styles.syarat}>
              4. Patologi Klinik
            </Text>
          </View>
        </View>
        <Gap height={30} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    height: 40,
    width: 40,
    borderRadius: 5,
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  keterangan: {
    paddingHorizontal: responsiveWidth(10),
  },
  pages: {
    backgroundColor: colors.white,
    flex: 1,
  },
  wrap: {
    marginHorizontal: 30,
  },
  card: {
    backgroundColor: colors.white,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    // marginHorizontal: 30,
    padding: responsiveHeight(30),
    borderRadius: 10,
    // backgroundColor: colors.white,
  },
  syaratCard: {
    backgroundColor: colors.white,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    padding: responsiveHeight(20),
    borderRadius: 10,
    borderBottomWidth: 5,
    borderBottomColor: colors.primary,
    borderTopWidth: 4,
    borderTopColor: colors.secondary,
  },
  text: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: colors.drak,
  },
  syaratTitle: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: colors.dark,
    marginBottom: 10,
    alignSelf: 'center',
  },
  header: {
    marginVertical: 15,
  },
  headerText: {
    fontSize: 16,
    fontFamily: fonts.primary.regular,
    color: colors.dark,
    marginBottom: 10,
    justifyContent: 'center',
    textAlign: 'center',
  },
  syarat: {
    fontSize: 14,
    fontFamily: fonts.primary.regular,
    color: colors.dark,
    textAlign: 'justify',
    marginVertical: 5,
  },
});

export default FasilitasPelayanan;
