import React, {useEffect, useRef, useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getKotaDetail} from '../../actions/RajaOngkirAction';
import {CardAlamat, Gap, Pilihan, Tombol} from '../../components/atoms';
import {dummyPesanans, dummyProfile} from '../../data';
import {
  colors,
  fonts,
  getData,
  numberWithCommas,
  responsiveWidth,
} from '../../utils';

const Checkout = props => {
  const [alamat, setAlamat] = useState('');
  const [provinsi, setProvinsi] = useState('');
  const [kota, setKota] = useState('');
  const [expedisi, setExpedisi] = useState([]);
  const [totalHarga, setTotalHarga] = useState(props.route.params.totalHarga);
  const [totalBerat, setTotalBerat] = useState(props.route.params.totalBerat);

  const dispatch = useDispatch();
  const data = useSelector(state => state.RajaOngkirReducer);
  const {kotaDetailLoading, kotaDetailData, kotaDetailError} = data;
 
  const mounted = useRef();
  useEffect(() => {
    if (!mounted.current) {
      getData('user').then(res => {
        const data = res;
        if (data) {
          setAlamat(data.alamat)
          dispatch(getKotaDetail(data.kota));
        } else {
          props.navigation.replace('Login');
        }
      });

      mounted.current = true;
    } else {
      if (kotaDetailData) {
        setProvinsi(kotaDetailData.province);
        setKota(kotaDetailData.type + ' ' + kotaDetailData.city_name);
      }
    }
  });

  return (
    <View style={styles.pages}>
      <View style={styles.isi}>
        <Text style={styles.textBold}>Apakah Benar Alamat Ini ?</Text>

        <CardAlamat alamat={alamat} kota={kota} provinsi={provinsi} />

        <View style={styles.totalHarga}>
          <Text style={styles.text}>Total Harga : </Text>
          <Text style={styles.text}>Rp. {numberWithCommas(totalHarga)}</Text>
        </View>

        <Pilihan label="Pilih Expedisi" datas={expedisi} />

        <Gap height={10} />

        <View style={styles.ongkir}>
          <Text style={styles.text}>Untuk Berat : {totalBerat} Kg</Text>
          <Text style={styles.textBold}>Rp. {numberWithCommas(15000)}</Text>
        </View>

        <View style={styles.ongkir}>
          <Text style={styles.text}>Estimasi Waktu : </Text>
          <Text style={styles.textBold}> 2-3 Hari</Text>
        </View>
      </View>

      <View style={styles.footer}>
        <View style={styles.totalHarga}>
          <Text style={styles.text}>Total Harga : </Text>
          <Text style={styles.text}>Rp. {numberWithCommas(totalHarga)}</Text>
        </View>

        <Tombol
          title="Check Out"
          type="textIcon"
          fontSize={18}
          padding={responsiveWidth(15)}
          icon="keranjang-putih"
          onPress={() => props.navigation.navigate('Checkout')}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  pages: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: 30,
    justifyContent: 'space-between',
  },
  isi: {
    paddingHorizontal: 30,
  },
  textBold: {
    fontFamily: 18,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  totalHarga: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 20,
  },
  text: {
    fontFamily: 18,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  ongkir: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  footer: {
    paddingHorizontal: 30,
    backgroundColor: colors.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    paddingBottom: 30,
  },
});

export default Checkout;
