import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getLimitJersey, getListJersey} from '../../actions/JerseyAction';
import {getListLiga} from '../../actions/LigaAction';
import {MedicalHistory} from '../../assets';
import {Gap} from '../../components/atoms';
import {
  BannerSlider,
  HeaderConponent,
  ListMenuHome,
} from '../../components/molecules';
import {fonts, responsiveHeight, responsiveWidth} from '../../utils';
import {colors} from '../../utils/colors';

const Home = ({navigation}) => {
  return (
    <View style={styles.page}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <HeaderConponent navigation={navigation} page="Home" />
        <BannerSlider />
        <View style={styles.wrap}>
          <Text style={styles.label}>Bantu Kami Menjadi Lebih Baik.</Text>

          <TouchableOpacity
            onPress={() => navigation.navigate('RegisterRawatJalan')}
            style={styles.card}>
            <View style={styles.container}>
              <Image source={MedicalHistory} style={styles.image} />
              <View style={styles.keterangan}>
                <Text style={styles.text}>Pendaftaran Rawat Jalan</Text>
                <Gap height={7} />
                <Text>Nikmati Kemudahan Akses Poliklinik Rawat Jalan</Text>
              </View>
            </View>
          </TouchableOpacity>
          <Gap height={10} />
          {/* <ListLiga /> */}

          <Text style={styles.label}>
            Pilih Menu <Text style={styles.labelBold}>Categori</Text>
          </Text>

          <Gap height={10} />
          {/* <ListJerseys /> */}
          <ListMenuHome />
        </View>
        <Gap height={100} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.white,
  },
  wrap: {
    marginHorizontal: 30,
    marginTop: 10,
  },
  image: {
    height: 40,
    width: 40,
    borderRadius: 5,
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  keterangan: {
    paddingHorizontal: responsiveWidth(10),
  },
  label: {
    fontSize: 16,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  labelBold: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  card: {
    backgroundColor: colors.white,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    padding: responsiveHeight(30),
    borderRadius: 10,
  },
  text: {
    fontSize: 16,
    fontFamily: fonts.primary.bold,
    color: colors.drak,
  },
});

export default Home;
