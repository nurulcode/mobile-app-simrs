import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  ActivityIndicator,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {useDispatch, useSelector} from 'react-redux';
import {getJadwalPoliDetail} from '../../actions/HomeAction';
import {Bed2, HealthCare, IconArrowRight, IndonesiaRupiah} from '../../assets';
import {Gap} from '../../components/atoms';
import {ListHistory} from '../../components/molecules';
import {dummyJadwalDokter, dummyJadwalDokterPoli} from '../../data';
import {
  colors,
  fonts,
  heightMobileUI,
  responsiveHeight,
  responsiveWidth,
} from '../../utils';

const ListJadwalDokterPoli = props => {
  const params = props.route.params;
  const dispatch = useDispatch();

  const PoliJadwalDetailRed = useSelector(
    state => state.PoliJadwalDetailReducer,
  );
  const {poliJadwalDetailData, poliJadwalDetailError, poliJadwalDetailLoading} =
    PoliJadwalDetailRed;

  useEffect(() => {
    if (params.id) {
      dispatch(getJadwalPoliDetail(params.id));
    }
  }, []);

  const [data, setData] = useState(dummyJadwalDokterPoli);
  return (
    <View style={styles.pages}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.wrap}>
          {poliJadwalDetailData.length ? (
            poliJadwalDetailData.map((data, index) => (
              <View style={styles.container} key={index}>
                <View style={styles.card}>
                  <Text style={styles.textTitle}>
                    {props.route.params.uraian}
                  </Text>
                  <Text style={styles.textTitle}>{data.dokter}</Text>
                  <Gap height={20} />
                  {data.jadwal.map((jadwal, index) => (
                    <View key={jadwal.id}>
                      <View style={styles.display}>
                        <Text style={styles.text}>Hari</Text>
                        <Text style={styles.text}>{jadwal.hari}</Text>
                      </View>
                      <View style={styles.display}>
                        <Text style={styles.text}>Pukul</Text>
                        <Text style={styles.text}>
                          {jadwal.mulai} - {jadwal.selesai}
                        </Text>
                      </View>
                      <View style={styles.display}>
                        <Text style={styles.text}>Kuota</Text>
                        <Text style={styles.text}>{jadwal.kuota}</Text>
                      </View>

                      <View
                        style={{
                          borderBottomColor: 'black',
                          borderBottomWidth: StyleSheet.hairlineWidth,
                          marginVertical: 10,
                        }}
                      />
                    </View>
                  ))}
                </View>
              </View>
            ))
          ) : poliJadwalDetailLoading ? (
            <View
              style={{
                marginTop: responsiveHeight(40),
              }}>
              <ActivityIndicator color={colors.primary} />
            </View>
          ) : (
            <View
              style={{
                marginTop: responsiveHeight(40),
                alignItems: 'center',
              }}>
              <Text>List Data Kosong</Text>
            </View>
          )}
        </View>
        <Gap height={20} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    paddingHorizontal: responsiveWidth(20),
    flex: 1,
  },
  pages: {
    backgroundColor: colors.white,
    flex: 1,
  },
  wrap: {
    marginHorizontal: 30,
  },
  container: {
    flexDirection: 'row',
    backgroundColor: colors.white,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    padding: responsiveHeight(10),
    borderRadius: 10,
    alignItems: 'center',
    borderEndWidth: 7,
    borderRightColor: colors.secondary,
  },
  text: {
    fontSize: 16,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  textTitle: {
    fontSize: RFValue(18, heightMobileUI),
    fontFamily: fonts.primary.regular,
    color: colors.primary,
    textAlign: 'center',
  },
  display: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default ListJadwalDokterPoli;
