import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  ScrollView,
  Keyboard,
  KeyboardAvoidingView,
  Alert,
} from 'react-native';

import {IlustrasiRegister2} from '../../assets';
import {Gap, Inputan, Pilihan, Tombol} from '../../components/atoms';
import {dummyProfile} from '../../data';
import {colors, fonts, responsiveHeight, responsiveWidth} from '../../utils';

import {useDispatch, useSelector} from 'react-redux';
import {getKotaList, getProvinsiList} from '../../actions/RajaOngkirAction';
import {registerUser} from '../../actions/AuthAction';

const Register2 = props => {
  const [kota, setKota] = useState(false);
  const [provinsi, setProvinsi] = useState(false);
  const [alamat, setAlamat] = useState('');

  const dispatch = useDispatch();
  const data = useSelector(state => state.RajaOngkirReducer);
  const {provinsiData, kotaData} = data;

  const dataRegister = useSelector(state => state.AuthReducer);
  const {registerLoading, registerData, registerError} = dataRegister;

  function changeProvinsi(provinsi_id) {
    setProvinsi(provinsi_id);
    dispatch(getKotaList(provinsi_id));
  }

  useEffect(() => {
    dispatch(getProvinsiList());

    if (registerData) {
      props.navigation.replace('MainApp');
    }

    if (registerError) {
      Alert.alert('Error', registerError);
    }
  }, [dispatch, dataRegister, registerError]);

  onContinue = () => {
    const {email, nama, nohp, password} = props.route.params;

    if (alamat && kota && provinsi) {
      const data = {
        nama,
        email,
        nohp,
        alamat,
        kota,
        provinsi,
        status: 'user',
      };

      dispatch(registerUser(data, password));
    } else {
      Alert.alert('Error', 'Alamat, Kota dan Provinsi tidak boleh kosong');
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.pages}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <ScrollView>
          <View>
            <View style={styles.btnBack}>
              <Tombol
                icon="arrow-left"
                onPress={() => props.navigation.goBack()}
              />
            </View>
            <View style={styles.ilustrasi}>
              <IlustrasiRegister2 />
              <Gap height={5} />
              <Text style={styles.title}>Isi Alamat</Text>
              <Text style={styles.title}>Lengkap Andaa</Text>

              <View style={styles.wrapperCircle}>
                <View style={styles.circleDisabled}></View>
                <Gap width={10} />
                <View style={styles.circlePrimary}></View>
              </View>
            </View>

            <View style={styles.card}>
              <Inputan
                label="Alamat"
                textarea
                value={alamat}
                onChangeText={alamat => setAlamat(alamat)}
              />

              <Pilihan
                label="Provinsi"
                datas={provinsiData ? provinsiData : []}
                selectedValue={provinsi}
                onValueChange={provinsi_id => changeProvinsi(provinsi_id)}
              />

              <Pilihan
                label="Kab/Kota"
                datas={kotaData ? kotaData : []}
                selectedValue={kota}
                onValueChange={kota => setKota(kota)}
              />
              <Gap height={25} />

              <Tombol
                title="Simpan"
                type="textIcon"
                icon="submit"
                padding={10}
                fontSize={18}
                onPress={() => onContinue()}
                loading={registerLoading}
                // onPress={() => props.navigation.navigate('MainApp')}
              />
            </View>
          </View>
        </ScrollView>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: 20,
  },
  ilustrasi: {
    alignItems: 'center',
  },

  title: {
    fontSize: 24,
    fontFamily: fonts.primary.light,
    color: colors.primary,
  },
  wrapperCircle: {
    flexDirection: 'row',
    marginTop: 10,
  },
  circlePrimary: {
    backgroundColor: colors.primary,
    width: responsiveWidth(11),
    height: responsiveWidth(11),
    borderRadius: 10,
  },
  circleDisabled: {
    backgroundColor: colors.border,
    width: responsiveWidth(11),
    height: responsiveWidth(11),
    borderRadius: 10,
  },

  card: {
    backgroundColor: colors.white,
    marginHorizontal: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    paddingHorizontal: 30,
    paddingBottom: 20,
    paddingTop: 10,
    borderRadius: 10,
    marginTop: 10,
    marginBottom: 10,
  },
  btnBack: {
    marginLeft: 30,
    position: 'absolute',
  },
});

export default Register2;
