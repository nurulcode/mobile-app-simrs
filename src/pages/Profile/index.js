import React, {useState} from 'react';
import {useEffect} from 'react';
import {View, Text, StyleSheet, Image, Alert} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {DefaultImage} from '../../assets';
import {ListMenu} from '../../components/molecules';
import {dummyMenu} from '../../data';
import {
  colors,
  fonts,
  getData,
  responsiveHeight,
  responsiveWidth,
} from '../../utils';
import {heightMobileUI} from '../../utils';

const Profile = ({navigation}) => {
  const [profile, setProfile] = useState(false);
  const [menus, setMenus] = useState(dummyMenu);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getUser();
    });

    return unsubscribe;
  }, [navigation]);

  const getUser = async () => {
    await getData('userInfo').then(res => {
      if (res) {
        setProfile(res);
      } else {
        navigation.goBack();
        Alert.alert('Error', 'Silahkan Login Untuk mengakses modul ini', [
          {
            text: 'Login',
            onPress: () => navigation.replace('Login'),
            style: 'login',
          },
          {text: 'Close'},
        ]);
      }
    });
  };

  return (
    <View style={styles.page}>
      <View style={styles.container}>
        <Image source={DefaultImage} style={styles.foto} />

        {profile && (
          <View style={styles.profile}>
            <Text style={styles.nama}>{profile.pasien.nama}</Text>
            <Text style={styles.desc}>{profile.pasien.no_rekam_medis}</Text>
            <Text style={styles.desc}>{profile.pasien.no_bpjs}</Text>
          </View>
        )}

        <ListMenu menus={menus} navigation={navigation} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  container: {
    position: 'absolute',
    bottom: 0,
    height: responsiveHeight(680),
    width: '100%',
    backgroundColor: colors.white,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
  },
  foto: {
    width: responsiveWidth(150),
    height: responsiveHeight(150),
    borderRadius: 40,
    alignSelf: 'center',
    marginTop: -responsiveWidth(75),
  },
  profile: {
    marginTop: 10,
    alignItems: 'center',
  },
  nama: {
    fontFamily: fonts.primary.bold,
    fontSize: RFValue(24, heightMobileUI),
    color: colors.drak,
  },
  desc: {
    fontFamily: fonts.primary.regular,
    fontSize: RFValue(20, heightMobileUI),
    color: colors.drak,
  },
});

export default Profile;
