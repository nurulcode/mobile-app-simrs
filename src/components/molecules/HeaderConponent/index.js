import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {saveKeywordJersey} from '../../../actions/JerseyAction';
import {getListKeranjang} from '../../../actions/KeranjangAction';
import {IconCari, IconKeranjang} from '../../../assets';
import {fonts, getData, responsiveHeight} from '../../../utils';
import {colors} from '../../../utils/colors';
import {Gap, Tombol} from '../../atoms';

const HeaderConponent = props => {
  const [search, setSearch] = useState('');
  const dispatch = useDispatch();

  // const selesaiCari = () => {
  //   dispatch(saveKeywordJersey(search));
  //   if (props.page !== 'ListJersey') {
  //     props.navigation.navigate('ListJersey');
  //   }

  //   setSearch('');
  // };

  // const data = useSelector(state => state.ListKeranjangReducer);
  // const {listKeranjangData, listKeranjangLoading, listKeranjangError} = data;

  // useEffect(() => {
  //   getData('user').then(res => {
  //     if (res) {
  //       dispatch(getListKeranjang(res.uid));
  //     }
  //   });
  // }, []);

  return (
    <View style={styles.container}>
      <View style={styles.wrap}>
        <View style={styles.dateContent}>
          <Text style={styles.date}>Selamat Datang.</Text>
        </View>
        {/* <View style={styles.search}>
          <IconCari />
          <TextInput
            placeholder="Cari . . ."
            style={styles.input}
            value={search}
            onChangeText={search => setSearch(search)}
            onSubmitEditing={() => selesaiCari()}
          />
        </View>
        <Gap width={10} />
        {listKeranjangData && (
          <Tombol
            icon="keranjang"
            totalKeranjang={Object.keys(listKeranjangData.pesanans).length}
            padding={10}
            onPress={() => props.navigation.navigate('Keranjang')}
          />
        )} */}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary,
    height: responsiveHeight(78),
  },
  wrap: {
    marginHorizontal: 30,
    marginTop: 15,
    flexDirection: 'row',
  },
  search: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: colors.white,
    borderRadius: 5,
    paddingLeft: 10,
    alignItems: 'center',
  },
  input: {
    fontSize: 16,
    color: colors.drak,
    fontFamily: fonts.primary.regular,
    flex: 1,
  },
  date: {
    fontSize: 18,
    color: colors.white,
    fontFamily: fonts.primary.regular,
  },
  dateContent: {
    alignItems: 'center',
  },
  loading: {
    flex: 1,
    marginTop: 10,
    marginBottom: 30,
  },
});

export default HeaderConponent;
