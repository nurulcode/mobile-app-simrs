import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { CardHistory } from '../../atoms';
 
const ListHistory = ({pesanans}) => {
    return (
        <View style={styles.container}>
            {
                pesanans.map((pesanan) => (
                    <CardHistory pesanan={pesanan} key={pesanan.id}/>
                ))
            }
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        marginHorizontal: 30,
        marginTop: 30
    }
});
 
 
export default ListHistory;