import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';
import {Slider1, Slider2, Slider11 , Slider22, Slider33} from '../../../assets';
import {colors, responsiveHeight, responsiveWidth} from '../../../utils';

const BannerSlider = () => {
  const [sliders, setSliders] = useState([Slider11, Slider22, Slider33]);

  return (
    <View style={styles.container}>
      <SliderBox
        images={sliders}
        autoplay
        circleLoop
        // sliderBoxHeight={responsiveHeight(132)}
        sliderBoxHeight={responsiveHeight(185)}
        ImageComponentStyle={styles.slider}
        dotStyle={styles.dotStyle}
        dotColor={colors.primary}
        imageLoadingColor={colors.primary}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: -15,
  },
  slider: {
    borderRadius: 10,
    width: responsiveWidth(354),
  },
  dotStyle: {
    width: 10,
    height: 5,
    borderRadius: 5,
  },
});

export default BannerSlider;
