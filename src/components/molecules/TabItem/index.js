import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {
  IconHistory2,
  IconHistoryAktif,
  IconHome,
  IconHomeAktif,
  IconJersey,
  IconJerseyAktif,
  IconProfile,
  IconProfileAktif,
} from '../../../assets';
import { fonts } from '../../../utils';
import { colors } from '../../../utils/colors';

const TabItem = props => {
  const Icon = () => {
    if (props.label == 'Home') {
      return props.isFocused ? <IconHomeAktif /> : <IconHome />;
    } else if (props.label == 'Jersey') {
      return props.isFocused ? <IconJerseyAktif /> : <IconJersey />;
    } else if (props.label == 'History') {
      return props.isFocused ? <IconHistoryAktif /> : <IconHistory2 />;
    } else if (props.label == 'Profile') {
      return props.isFocused ? <IconProfileAktif /> : <IconProfile />;
    } else {
      return <IconHomeAktif />;
    }
  };

  return (
    <TouchableOpacity
      accessibilityRole="button"
      accessibilityState={props.isFocused ? {selected: true} : {}}
      onPress={props.onPress}
      onLongPress={props.onLongPress}
      style={styles.container(props.isFocused)}>
      <Icon />
      <Text style={styles.text(props.isFocused)}>{props.label}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: (isFocused) => ({
    backgroundColor: isFocused ? colors.secondary : colors.primary ,
    alignItems: 'center',
    flex: 1,
    paddingVertical:  7,
  }),
  text: isFocused => ({
    color: isFocused ? colors.white : colors.secondary,
    fontSize: 12,
    fontFamily: fonts.primary.regular,
  }),
});

export default TabItem;
