import { useNavigation } from '@react-navigation/native';
import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import { Bed, DefaultImage, Doctor, Folder, Patient, PatientPoli, Registration } from '../../../assets';
import {colors, fonts, responsiveHeight, responsiveWidth} from '../../../utils';


const ListMenuHome = () => {
  const navigation = useNavigation()
  return (
    <View style={styles.container}>
      <View style={styles.wrap}>
        <TouchableOpacity style={styles.card} onPress={(() => navigation.navigate('DisplayTempatTidur'))}>
          <View style={styles.item}>
          <Image source={Bed} style={styles.logo} />
          <Text style={styles.text}>Display Tempat Tidur</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.card} onPress={(() => navigation.navigate('MonitorAntrean'))}>
          <View style={styles.item}>
          <Image source={Patient} style={styles.logo} />
          <Text style={styles.text}>Monitor Antrean</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.card} onPress={(() => navigation.navigate('JadwalDokter'))}>
          <View style={styles.item}>
          <Image source={Doctor} style={styles.logo} />
          <Text style={styles.text}>Jadwal Dokter</Text>
          </View>
        </TouchableOpacity>
      </View>

      <View style={styles.wrap}>
        <TouchableOpacity style={styles.card} onPress={(() => navigation.navigate('KuotaPoli'))}>
          <View style={styles.item}>
          <Image source={PatientPoli} style={styles.logo} />
          <Text style={styles.text}>Kuota Poli</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.card} onPress={(() => navigation.navigate('FasilitasPelayanan'))}>
          <View style={styles.item}>
          <Image source={Registration} style={styles.logo} />
          <Text style={styles.text}>Fasilitas Pelayanan</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.card} onPress={(() => navigation.navigate('KontakKami'))}>
          <View style={styles.item}>
          <Image source={Folder} style={styles.logo} />
          <Text style={styles.text}>Kontak Kami</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  wrap: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginBottom: 18,
  },
  card: {
    width: responsiveWidth(110),
    height: responsiveWidth(110),
    backgroundColor: colors.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    padding: 10,
    borderRadius: 15,
    marginTop: 10,
    justifyContent: 'center'
  },
  text: {
    marginTop: 5,
    fontSize: 13,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
    textAlign: 'center',
  },
  logo: {
    alignSelf: 'center',
    borderRadius: 15,
    width: responsiveWidth(45),
    height: responsiveHeight(60),
  },
});


export default ListMenuHome;
