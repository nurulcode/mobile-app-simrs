import React, {useState} from 'react';
import {View, Text, StyleSheet, Modal} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import {SliderBox} from 'react-native-image-slider-box';

import {colors, responsiveHeight, responsiveWidth} from '../../../utils';

const JerseySlider = ({images}) => {
  const [openImage, setOpenImage] = useState(false);
  const [previewImage, setPreviewImage] = useState(false);

  const clickPreview = index => {
    setOpenImage(true);
    setPreviewImage([
      {
        url: '',
        props: {
          // Or you can set source directory.
          source: images[index],
        },
      },
    ]);
  };

  return (
    <View>
      <SliderBox
        images={images}
        sliderBoxHeight={responsiveHeight(430)}
        ImageComponentStyle={styles.jersey}
        dotStyle={styles.dotStyle}
        dotColor={colors.primary}
        imageLoadingColor={colors.primary}
        onCurrentImagePressed={index => clickPreview(index)}
      />

      <Modal visible={openImage} transparent={true}>
        <ImageViewer
          imageUrls={previewImage}
          backgroundColor={colors.primary}
          onClick={() => setOpenImage(false)}
          enableSwipeDown
          onSwipeDown={() => setOpenImage(false)}
        />
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  jersey: {
    marginTop: 25,
    width: responsiveWidth(344),
  },
  dotStyle: {
    marginTop: -50,
  },
});

export default JerseySlider;
