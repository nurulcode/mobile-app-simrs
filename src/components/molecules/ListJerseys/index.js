import React from 'react';
import {View, Text, StyleSheet, ActivityIndicator} from 'react-native';
import {useSelector} from 'react-redux';
import {colors} from '../../../utils';
import {CardJersey} from '../../atoms';

const ListJerseys = () => {
  const data = useSelector(state => state.JerseyReducer);
  const {jerseyLoading, jerseyData} = data;

  return (
    <View style={styles.container}>
      {jerseyData ? (
        Object.keys(jerseyData).map(key => (
          <CardJersey jersey={jerseyData[key]} key={key} />
        ))
      ) : jerseyLoading ? (
        <View
          style={{
            marginTop: responsiveHeight(40),
          }}>
          <ActivityIndicator color={colors.primary} />
        </View>
      ) : (
        <View
          style={{
            marginTop: responsiveHeight(40),
            alignItems: 'center',
          }}>
          <Text>List Data Kosong</Text>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  loading: {
    flex: 1,
    marginTop: 10,
    marginBottom: 30,
  },
});

export default ListJerseys;
