import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import { useDispatch } from 'react-redux';
import { removeJerseyByLiga } from '../../../actions/JerseyAction';
import {colors} from '../../../utils/colors';
import TabItem from '../TabItem';

const BottomNavigator = ({state, descriptors, navigation}) => {
  const dispatch = useDispatch();

  return (
    <View style={style.container}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }

          if (route.name !== "ListJersey") {
            dispatch(removeJerseyByLiga())
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TabItem
            key={index}
            label={label}
            isFocused={isFocused}
            onLongPress={onLongPress}
            onPress={onPress}
          />
        );
      })}
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    marginHorizontal: 25,
    flexDirection: 'row',
    position: 'absolute',
    bottom: 20,
    left: 0,
    right: 0,
    backgroundColor: colors.primary,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
});

export default BottomNavigator;
