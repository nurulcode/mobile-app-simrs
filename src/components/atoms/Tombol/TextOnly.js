import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {IconKeranjang} from '../../../assets';
import {colors, fonts} from '../../../utils';

const TextOnly = ({padding, title, onPress, fontSize, color}) => {
  return (
    <TouchableOpacity
      style={styles.container(padding, color)}
      onPress={onPress}>
      <Text style={styles.text(fontSize)}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: (padding, color) => ({
    padding: padding,
    backgroundColor: color ? color : colors.primary,
    borderRadius: 5,
  }),
  text: fontSize => ({
    color: colors.white,
    textAlign: 'center',
    fontSize: fontSize ? fontSize : 13,
    fontFamily: fonts.primary.regular,
  }),
});

export default TextOnly;
