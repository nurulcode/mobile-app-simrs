import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {IconBack, IconKeranjang, IconSubmit} from '../../../assets';
import {colors} from '../../../utils';
import TombolLoading from './Loading';
import TextIcon from './TextIcon';
import TextOnly from './TextOnly';

const Tombol = props => {
  const Icon = () => {
    if (props.icon == 'keranjang') {
      return <IconKeranjang />;
    } else if (props.icon == 'arrow-left') {
      return <IconBack />;
    } else if (props.icon == 'submit') {
      return <IconSubmit />;
    }

    return <IconKeranjang />;
  };

  if (props.loading) {
    return <TombolLoading {...props}/>
  }

  if (props.type == 'text') {
    return <TextOnly {...props} />;
  } else if (props.type == 'textIcon') {
    return <TextIcon {...props} />;
  }

  return (
    <TouchableOpacity style={styles.container(props)} onPress={props.onPress}>
      <Icon />
      {props.totalKeranjang && (
        <View style={styles.notif}>
          <Text style={styles.textNotif}>{props.totalKeranjang}</Text>
        </View>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: props => ({
    padding: props.padding,
    backgroundColor: colors.white,
    borderRadius: 5,
  }),
  notif: {
    position: 'absolute',
    top: 5,
    right: 5,
    backgroundColor: 'red',
    borderRadius: 10,
    padding: 3,
  },
  textNotif: {
    fontSize: 8,
    color: colors.white,
  },
});

export default Tombol;
