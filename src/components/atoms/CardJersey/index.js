import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {colors, fonts, responsiveWidth} from '../../../utils';
import Tombol from '../Tombol';

const CardJersey = ({jersey}) => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.card}
        onPress={() => navigation.navigate('JerseyDetail', {jersey})}>
        {/* <Image source={{ uri: jersey.gambar[0] }} style={styles.gambar} /> */}
        <Text style={styles.text}>{jersey.nama}</Text>
      </TouchableOpacity>

      {/* <Tombol
        type="text"
        title="Detail"
        padding={7}
        onPress={() => navigation.navigate('JerseyDetail', {jersey})}
      /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
  },
  card: {
    backgroundColor: colors.yellow,
    width: responsiveWidth(150),
    alignItems: 'center',
    padding: 10,
    borderRadius: 10,
    marginBottom: 10,
  },
  gambar: {
    width: 124,
    height: 124,
  },
  text: {
    fontFamily: fonts.primary.regular,
    fontSize: 13,
    textTransform: 'capitalize',
    color: colors.drak,
    textAlign: 'center',
  },
});

export default CardJersey;
