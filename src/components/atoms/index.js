import Gap from './Gap';
import Tombol from './Tombol';
import CardLiga from './CardLiga';
import CardJersey from './CardJersey';
import CardMenu from './CardMenu';
import Inputan from './Inputan';
import Pilihan from './Pilihan';
import CardKeranjang from './CardKeranjang';
import CardAlamat from './CardAlamat';
import CardHistory from './CardHistory';

export {
  Gap,
  Tombol,
  CardLiga,
  CardJersey,
  CardMenu,
  Inputan,
  Pilihan,
  CardKeranjang,
  CardAlamat,
  CardHistory,
};
