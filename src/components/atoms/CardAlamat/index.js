import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {colors, fonts} from '../../../utils';

const CardAlamat = ({alamat, kota, provinsi}) => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Alamat Saya : </Text>
      <Text style={styles.alamat}>{alamat}</Text>
      <Text style={styles.alamat}>Kota/Kab. {kota}</Text>
      <Text style={styles.alamat}>provinsi {provinsi}</Text>
      <TouchableOpacity
        onPress={() => navigation.navigate('EditProfile')}>
        <Text style={styles.ubahAlamat}>Ubah Alamat</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    borderRadius: 10,
    marginTop: 10,
    padding: 15,
  },
  title: {
    color: colors.drak,
    fontFamily: fonts.primary.regular,
    fontSize: 14,
    marginBottom: 10,
  },
  alamat: {
    color: colors.drak,
    fontFamily: fonts.primary.regular,
    fontSize: 14,
  },
  ubahAlamat: {
    color: colors.drak,
    fontFamily: fonts.primary.regular,
    fontSize: 14,
    color: colors.primary,
    alignSelf: 'flex-end',
  },
});

export default CardAlamat;
