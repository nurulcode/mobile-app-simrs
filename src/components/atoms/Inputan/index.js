import React from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import {colors, fonts} from '../../../utils';

const Inputan = ({
  textarea,
  width,
  height,
  fontSize,
  placeholder,
  label,
  value,
  secureTextEntry,
  keyboardType,
  onChangeText,
  disabled,
  onTouchStart,
  showSoftInputOnFocus,
  maxLength
}) => {
  if (textarea) {
    // textarea
    return (
      <View style={styles.container}>
        <Text style={styles.label(fontSize)}>{label} : </Text>
        <TextInput
          style={styles.inputTextArea(fontSize)}
          multiline={true}
          numberOfLines={3}
          value={value}
          onChangeText={onChangeText}
          editable={disabled ? false : true}
          placeholder={placeholder}
          
        />
      </View>
    );
  }
  // input
  return (
    <View style={styles.container}>
      <Text style={styles.label(fontSize)}>{label} : </Text>
      <TextInput
        style={styles.input(width, height, fontSize)}
        value={value}
        secureTextEntry={secureTextEntry}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        editable={disabled ? false : true}
        placeholder={placeholder}
        onTouchStart={onTouchStart}
        showSoftInputOnFocus={showSoftInputOnFocus}
        maxLength={maxLength}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
  },
  label: fontSize => ({
    fontSize: fontSize ? fontSize : 18,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
    marginBottom: 4
  }),
  input: (width, height, fontSize) => ({
    fontSize: fontSize ? fontSize : 18,
    fontFamily: fonts.primary.regular,
    width: width,
    height: height,
    borderWidth: 1,
    // borderRadius: 5,
    borderColor: colors.border,
    paddingVertical: 5,
    paddingHorizontal: 10,
  }),
  inputTextArea: fontSize => ({
    fontSize: fontSize ? fontSize : 18,
    fontFamily: fonts.primary.regular,
    borderWidth: 1,
    // borderRadius: 5,
    borderColor: colors.border,
    paddingVertical: 5,
    paddingHorizontal: 10,
    textAlignVertical: 'top',
  }),
});

export default Inputan;
