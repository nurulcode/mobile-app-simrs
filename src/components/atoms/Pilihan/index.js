import React, {useState} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import {colors, fonts, responsiveHeight} from '../../../utils';
import {Picker} from '@react-native-picker/picker';

const Pilihan = ({
  datas,
  width,
  height,
  fontSize,
  label,
  selectedValue,
  onValueChange,
}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.label(fontSize)}>{label} : </Text>
      <View style={styles.wrapperPicker}>
        <Picker
          style={styles.picker(width, height, fontSize)}
          selectedValue={selectedValue}
          onValueChange={onValueChange}>
          <Picker.Item label="--Pilih--" value="" />
          {datas.map((item, index) => {
            if (label == 'Provinsi') {
              return (
                <Picker.Item
                  label={item.province}
                  value={item.province_id}
                  key={item.province_id}
                />
              );
            } else if (label == 'Kab/Kota') {
              return (
                <Picker.Item
                  label={item.type + ' ' + item.city_name}
                  value={item.city_id}
                  key={item.city_id}
                />
              );
            } else if (label == 'Poliklinik') {
              return (
                <Picker.Item
                  label={item.uraian}
                  value={item.id}
                  key={item.id}
                />
              );
            } else if (label == 'Dokter') {
              return (
                <Picker.Item
                  label={item.nama}
                  value={item}
                  key={item.id}
                />
              );
            } else {
              return <Picker.Item label={item} value={item} key={index} />;
            }
          })}
        </Picker>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
  },
  label: fontSize => ({
    fontSize: fontSize ? fontSize : 18,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  }),
  picker: (width, height, fontSize) => ({
    fontSize: fontSize ? fontSize : 18,
    fontFamily: fonts.primary.regular,
    width: width,
    height: height ? height : responsiveHeight(46),
    marginTop: -9,
    marginBottom: 8,
    color: colors.drak,
  }),
  wrapperPicker: {
    borderWidth: 1,
    // borderRadius: 5,
    borderColor: colors.border,
  },
});

export default Pilihan;
