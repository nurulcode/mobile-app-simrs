import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {useDispatch} from 'react-redux';
import {getJerseyByLiga} from '../../../actions/JerseyAction';
import {colors, responsiveHeight, responsiveWidth} from '../../../utils';

const CardLiga = ({liga, id}) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const toJerseyByLiga = (id, namaLiga) => {
    dispatch(getJerseyByLiga(id, namaLiga));
    navigation.navigate('ListJersey');
  };

  return (
    <View>
      <TouchableOpacity
        style={styles.container}
        onPress={() => toJerseyByLiga(id, liga.namaLiga)}>
        <Image source={{uri: liga.image}} style={styles.logo} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    padding: 10,
    borderRadius: 15,
    marginTop: 10,
    alignItems: 'center'
  },
  logo: {
    width: responsiveWidth(57),
    height: responsiveHeight(70),
  },
});

export default CardLiga;
